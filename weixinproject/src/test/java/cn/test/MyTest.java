package cn.test;


import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.management.relation.RoleList;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.toe.bean.Administrators;
import cn.toe.bean.Article;
import cn.toe.bean.Association;
import cn.toe.bean.Astyle;
import cn.toe.bean.Authority;
import cn.toe.bean.Galaxy;
import cn.toe.bean.Planet;
import cn.toe.bean.Role;
import cn.toe.bean.WeixinUserInfo;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;


public class MyTest {

	
	private ApplicationContext context = null;
	@Test
	public void  mj() {
		ApplicationContext context = new ClassPathXmlApplicationContext("spring-hibernate.xml");
		System.out.println(context);

		SessionFactory sessionFactory = context.getBean(SessionFactory.class);
		System.out.println(sessionFactory);

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		

//		WeixinUserInfo user1 = new WeixinUserInfo("001", 1, "1", "2", 1, "222", "222", "222", "222", "222");
//		String hql = "from Astyle a where a.astyleId=?";
//		Query query = session.createQuery(hql);//此时使用的是hql语句
//		query.setInteger(0, 1);
//		Astyle astyle = (Astyle) query.uniqueResult();
//		user1.addAstyle(astyle);
//		session.save(user1);
//		String hql = "from WeixinUserInfo";
//		Query query = session.createQuery(hql);
//		WeixinUserInfo user2 = (WeixinUserInfo) query.list().get(0);
//		System.out.println(user2.getNickName());
		//Sstar表
//		Date date = new Date();
//		Sstar sstar = new Sstar("新型", date, date, "sds", "dddd");
//		session.save(sstar);
		String hql = "from WeixinUserInfo w where w.userId=?";
		Query query = session.createQuery(hql);//此时使用的是hql语句
		query.setInteger(0, 3);
		WeixinUserInfo weixinUserInfo =  (WeixinUserInfo)query.uniqueResult();
		System.out.println(weixinUserInfo.getNickName());
		hql = "update WeixinUserInfo w set w.openId=:openId,w.subscribe=:subscribe,w.subscribeTime=:subscribeTime,w.nickName=:nickName,w.sex=:sex,w.country=:country,w.province=:province,w.city=:city,w.language=:language,w.headImgUrl=:headImgUrl where w.userId=:userId";
		query = session.createQuery(hql);
		query.setString("openId",weixinUserInfo.getOpenId());
		query.setInteger("subscribe",weixinUserInfo.getSubscribe());
		query.setString("subscribeTime",weixinUserInfo.getSubscribeTime());
		query.setString("nickName","庄书煌");
		query.setInteger("sex",weixinUserInfo.getSex());
		query.setString("country",weixinUserInfo.getCountry());
		query.setString("province",weixinUserInfo.getProvince());
		query.setString("city",weixinUserInfo.getCity());
		query.setString("language",weixinUserInfo.getLanguage());
		query.setString("headImgUrl",weixinUserInfo.getHeadImgUrl());
		query.setInteger("userId",weixinUserInfo.getUserId());
		System.out.println(query.executeUpdate());
		tx.commit();
		session.close();
	}
	
	
	
	//删除记录测试
	@Test
	public void test3() {
		context = new ClassPathXmlApplicationContext("spring-hibernate.xml");
		System.out.println(context);

		SessionFactory sessionFactory = context.getBean(SessionFactory.class);
		System.out.println(sessionFactory);

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		String hql = "from Authority a where a.authorityparent!=null";
		Query query = session.createQuery(hql);
		System.out.println(query.list().size());
		Authority authority = (Authority) query.list().get(0);
		List<Authority> list = authority.getAuthoritys();
		System.out.println(list.size());
		tx.commit();
		session.close();
	}
	
	@Test
	public void test2() {
		context = new ClassPathXmlApplicationContext("spring-hibernate.xml");
		System.out.println(context);

		SessionFactory sessionFactory = context.getBean(SessionFactory.class);
		System.out.println(sessionFactory);

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		String hql = "delete Planet p where p.planetid=?";
		Query query = session.createQuery(hql);
		query.setInteger(0, 1);
		System.out.println(query.executeUpdate());
		tx.commit();
		session.close();
	}
	
	@Test
	public void test5() {
		context = new ClassPathXmlApplicationContext("spring-hibernate.xml");
		System.out.println(context);

		SessionFactory sessionFactory = context.getBean(SessionFactory.class);
		System.out.println(sessionFactory);

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		String hql = "from Role r where r.role=?";
		Query query = session.createQuery(hql);//此时使用的是hql语句
		query.setString(0, "001");
		Role role = (Role) query.uniqueResult();
		System.out.println(role.getContent());
		role.setContent("庄书煌1");
		
		hql = "update Role r set r.content=:content where r.role=:role";
		query = session.createQuery(hql);
		query.setString("content", role.getContent());
		query.setInteger("role", role.getRole());
		System.out.println(query.executeUpdate());
		
		
		
		tx.commit();
		session.close();
	}
	
	
	@Test
	public void test6() {
		context = new ClassPathXmlApplicationContext("spring-hibernate.xml");
		System.out.println(context);

		SessionFactory sessionFactory = context.getBean(SessionFactory.class);
		System.out.println(sessionFactory);

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		String hql = "from Galaxy g where g.note=?";
		Query query = session.createQuery(hql);//此时使用的是hql语句
		query.setString(0, "111");
		Galaxy galaxy = (Galaxy) query.uniqueResult();
		System.out.println(galaxy.getGalaxyName());
		galaxy.setGalaxyIN("100");

		hql = "update Galaxy g set g.galaxyName=:galaxyName,g.galaxyIN=:galaxyIN,g.galaxyNum=:galaxyNum,g.type=:type,g.img=:img,g.fyear=:fyear,g.note=:note,g.ruler=:ruler where g.galaxyid=:galaxyid";
		query = session.createQuery(hql);
		query.setString("galaxyName",galaxy.getGalaxyName());
		query.setString("galaxyIN",galaxy.getGalaxyIN());
		query.setString("galaxyNum",galaxy.getGalaxyNum());
		query.setString("type",galaxy.getType());
		query.setString("img",galaxy.getImg());
		query.setDate("fyear",galaxy.getFyear());
		query.setString("note",galaxy.getNote());
		query.setInteger("ruler",galaxy.getRuler().getPlanetid());
		query.setInteger("galaxyid",galaxy.getGalaxyid());
		System.out.println(query.executeUpdate());
		
		
		tx.commit();
		session.close();
	}
	
	@Test
	public void test7() {
		context = new ClassPathXmlApplicationContext("spring-hibernate.xml");
		System.out.println(context);

		SessionFactory sessionFactory = context.getBean(SessionFactory.class);
		System.out.println(sessionFactory);

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		String hql = "from Astyle g where g.astyleId=?";
		Query query = session.createQuery(hql);//此时使用的是hql语句
		query.setInteger(0, 1);
		Astyle astyle = (Astyle) query.uniqueResult();
		System.out.println(astyle.getSname());

		hql = "update Astyle a set a.sname=:sname where a.astyleId=:astyleId";
		query = session.createQuery(hql);
		query.setString("sname",astyle.getSname());
		query.setInteger("astyleId", astyle.getAstyleId());
		System.out.println(query.executeUpdate());
		
		
		tx.commit();
		session.close();
	}
	
	
	@Test
	public void test8() {
		context = new ClassPathXmlApplicationContext("spring-hibernate.xml");
		System.out.println(context);

		SessionFactory sessionFactory = context.getBean(SessionFactory.class);
		System.out.println(sessionFactory);

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		String hql = "from Association a where a.associationId=?";
		Query query = session.createQuery(hql);//此时使用的是hql语句
		query.setInteger(0, 1);
		Association association = (Association) query.uniqueResult();
		System.out.println(association.getAname());

		hql = "update Association a set a.aname=:aname,a.url=:url,a.type=:type,a.address=:address,a.phone=:phone,a.note=:note,a.cdate=:cdate,a.ceo=:ceo where a.associationId=:associationId";
		query = session.createQuery(hql);
		query.setString("aname",association.getAname());
		query.setString("url",association.getUrl());
		query.setString("type",association.getType());
		query.setString("address",association.getAddress());
		query.setString("phone",association.getPhone());
		query.setString("note",association.getNote());
		query.setDate("cdate",association.getCdate());
		query.setString("ceo",association.getCeo());
		query.setInteger("associationId",association.getAssociationId());
		System.out.println(query.executeUpdate());
		
		
		tx.commit();
		session.close();
	}
	
	@Test
	public void test9() {
		context = new ClassPathXmlApplicationContext("spring-hibernate.xml");
		System.out.println(context);

		SessionFactory sessionFactory = context.getBean(SessionFactory.class);
		System.out.println(sessionFactory);

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		String hql = "from Article a where a.articleId=?";
		Query query = session.createQuery(hql);//此时使用的是hql语句
		query.setInteger(0, 1);
		Article article = (Article) query.uniqueResult();
		System.out.println(article.getTitle());
		article.setTitle("好的人生需要好的形象2");
//		session.update(article);

		hql = "update Article a set a.title=:title,a.institutions=:institutions,a.editor=:editor,a.img=:img,a.note=:note where a.articleId=:articleId";
		//,a.astyle=:astyle
		query = session.createQuery(hql);
		query.setString("title",article.getTitle());
		query.setString("institutions",article.getInstitutions());
		query.setString("editor",article.getEditor());
		query.setString("img",article.getImg());
		query.setString("note",article.getNote());
		//query.setInteger("astyle",article.getAstyle().getAstyleId());
		query.setInteger("articleId",article.getArticleId());
		System.out.println(query.executeUpdate());
		
		
		tx.commit();
		session.close();
	}
	
	@Test
	public void test10() {
		context = new ClassPathXmlApplicationContext("spring-hibernate.xml");
		System.out.println(context);

		SessionFactory sessionFactory = context.getBean(SessionFactory.class);
		System.out.println(sessionFactory);

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		String hql = "from Administrators a where a.adminid=?";
		Query query = session.createQuery(hql);//此时使用的是hql语句
		query.setInteger(0, 4);
		Administrators administrators = (Administrators) query.uniqueResult();
		System.out.println(administrators.getAdname());
		administrators.setAdname("好的人生需要好的形象2");

		hql = "update Administrators a set a.role=:role,a.adname=:adname,a.passWord=:passWord,a.documentType=:documentType,a.documentId=:documentId,a.email=:email,a.tphone=:tphone where a.adminid=:adminid";
		query = session.createQuery(hql);
		query.setBigInteger("role",null);
		query.setString("adname",administrators.getAdname());
		query.setString("passWord",administrators.getPassWord());
		query.setString("documentType",administrators.getDocumentType());
		query.setString("documentId",administrators.getDocumentId());
		query.setString("email",administrators.getEmail());
		query.setString("tphone",administrators.getTphone());
		query.setInteger("adminid",administrators.getAdminid());
		System.out.println(query.executeUpdate());
		
		
		
		tx.commit();
		session.close();
	}
	
	@Test
	public void test11() {
		List<Role> roles = new ArrayList<>();
		roles.add(new Role(1, "1"));
		roles.add(new Role(2, "2"));
		JsonConfig config = new JsonConfig();
		config.setExcludes(new String[]{"administratorss","authority","authoritys"});
		System.out.println(JSONArray.fromObject(roles, config));
	}
	@Test
	public void test12() {
		context = new ClassPathXmlApplicationContext("spring-hibernate.xml");
		System.out.println(context);

		SessionFactory sessionFactory = context.getBean(SessionFactory.class);
		System.out.println(sessionFactory);

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		String hql = "from Administrators a where a.adminid=?";
		Query query = session.createQuery(hql);//此时使用的是hql语句
		query.setInteger(0, 1);
		Administrators administrators = (Administrators) query.uniqueResult();
		System.out.println(administrators.getAdname());
		
		hql = "from Role r where r.role=?";
		query = session.createQuery(hql);//此时使用的是hql语句
		query.setString(0, "1");
		Role role = (Role) query.uniqueResult();
		System.out.println(role.getContent());
		
		administrators.setRole(role);
		
		tx.commit();
		session.close();
	}
	
	@Test
	public void test13() {
		context = new ClassPathXmlApplicationContext("spring-hibernate.xml");
		System.out.println(context);

		SessionFactory sessionFactory = context.getBean(SessionFactory.class);
		System.out.println(sessionFactory);

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "from Role r where r.role=?";
		Query query = session.createQuery(hql);//此时使用的是hql语句
		query.setInteger(0, 1);
		Role role = (Role) query.uniqueResult();
		System.out.println(role.getContent());
		
		hql = "update Role r set r.role=:role1 where r.role=:role2";
		query = session.createQuery(hql);
		query.setInteger("role1", 2);
		query.setInteger("role2", role.getRole());
		System.out.println(query.executeUpdate());
		
		tx.commit();
		session.close();
	}
	
	@Test
	public void test14() {
		List<Administrators> users= new ArrayList<>();
		users.add(new Administrators(1, "2", "3", "", "", "", ""));
		users.add(new Administrators(2, "2", "3", "", "", "", ""));
		JsonConfig config = new JsonConfig();
		config.setExcludes(new String[]{"weixinUserInfo","role"});
		System.out.println(JSONArray.fromObject(users, config));
		
	}
	
	@Test
	public void test15() {
		List<Astyle> astyles = new ArrayList<>();
		Astyle a=new Astyle("1");
		astyles.add(a);
		astyles.add(new Astyle("2"));
		WeixinUserInfo weixinUserInfo = new WeixinUserInfo();
		a.addPlanet(weixinUserInfo);
		JsonConfig config = new JsonConfig();
		config.setExcludes(new String[]{"weixinUserInfos","articles"});
		JSONArray json = JSONArray.fromObject(astyles, config);
		for(int i = 0;i<astyles.size();i++){
			List<WeixinUserInfo> weixinUserInfos = astyles.get(i).getWeixinUserInfos();
			List<Article> articles = astyles.get(i).getArticles();
			json.getJSONObject(i).put("weixinUserInfosSize", weixinUserInfos.size());
			json.getJSONObject(i).put("articlesSize", articles.size());
		}
		System.out.println(json);
	}
}
