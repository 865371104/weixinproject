package cn.toe.service;

import java.util.List;

import cn.toe.bean.Administrators;


public interface AdministratorsService {
	public void addAdministrators(Administrators administrators);  
    
	public List<Administrators> getAllAdministrators();  
	    
	public boolean delAdministrators(Integer id);  
	  
	public Administrators getAdministrators(Integer id);  
	
	public Administrators getAdministratorsByName(String name); 
	
	public boolean updateAdministrators(Administrators administrators);
}
