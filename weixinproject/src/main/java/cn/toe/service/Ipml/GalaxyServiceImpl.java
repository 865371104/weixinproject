package cn.toe.service.Ipml;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.toe.bean.Galaxy;
import cn.toe.dao.GalaxyDao;
import cn.toe.service.GalaxyService;

@Service
public class GalaxyServiceImpl implements GalaxyService{

	@Autowired
	private GalaxyDao galaxyDao;
	
	@Override
	public void addGalaxy(Galaxy galaxy) {
		// TODO Auto-generated method stub
		galaxyDao.addGalaxy(galaxy);
	}

	@Override
	public List<Galaxy> getAllGalaxy() {
		// TODO Auto-generated method stub
		return galaxyDao.getAllGalaxy();
	}

	@Override
	public boolean delGalaxy(Integer id) {
		// TODO Auto-generated method stub
		return galaxyDao.delGalaxy(id);
	}

	@Override
	public Galaxy getGalaxy(Integer id) {
		// TODO Auto-generated method stub
		return galaxyDao.getGalaxy(id);
	}

	@Override
	public boolean updateGalaxy(Galaxy galaxy) {
		// TODO Auto-generated method stub
		return galaxyDao.updateGalaxy(galaxy);
	}

}
