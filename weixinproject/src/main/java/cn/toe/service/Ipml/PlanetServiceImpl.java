package cn.toe.service.Ipml;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.toe.bean.Galaxy;
import cn.toe.bean.Planet;
import cn.toe.dao.PlanetDao;
import cn.toe.service.PlanetService;

@Service
public class PlanetServiceImpl implements PlanetService {

	@Autowired
	private PlanetDao planetDao;

	@Override
	public void addPlanet(Planet planet) {
		// TODO Auto-generated method stub
		planetDao.addPlanet(planet);
	}

	@Override
	public List<Planet> getAllPlanet() {
		// TODO Auto-generated method stub
		return planetDao.getAllPlanet();
	}

	@Override
	public List<Planet> getPlanetbyGa(Integer id) {
		// TODO Auto-generated method stub
		return planetDao.getPlanetbyGa(id);
	}

	@Override
	public boolean delPlanet(Integer id) {
		// TODO Auto-generated method stub
		return planetDao.delPlanet(id);
	}

	@Override
	public Planet getPlanet(Integer id) {
		// TODO Auto-generated method stub
		return planetDao.getPlanet(id);
	}

	@Override
	public boolean updatePlanet(Planet planet) {
		// TODO Auto-generated method stub
		return planetDao.updatePlanet(planet);
	}
	
	

}
