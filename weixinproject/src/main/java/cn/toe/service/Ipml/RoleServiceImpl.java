package cn.toe.service.Ipml;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.toe.bean.Role;
import cn.toe.dao.RoleDao;
import cn.toe.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDao roleDao;

	@Override
	public void addRole(Role role) {
		// TODO Auto-generated method stub
		roleDao.addRole(role);
	}

	@Override
	public List<Role> getAllRole() {
		// TODO Auto-generated method stub
		return roleDao.getAllRole();
	}

	@Override
	public boolean delRole(Integer id) {
		// TODO Auto-generated method stub
		return roleDao.delRole(id);
	}

	@Override
	public Role getRole(Integer id) {
		// TODO Auto-generated method stub
		return roleDao.getRole(id);
	}

	@Override
	public Role getRoleByName(String name) {
		// TODO Auto-generated method stub
		return roleDao.getRoleByname(name);
	}
	
	@Override
	public boolean updateRole(Role role) {
		// TODO Auto-generated method stub
		return roleDao.updateRoleofhql(role);
	}
	
	

}
