package cn.toe.service.Ipml;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.toe.bean.Astyle;
import cn.toe.dao.AstyleDao;
import cn.toe.service.AstyleService;

@Service
public class AstyleServiceImpl implements AstyleService {

	@Autowired
	private AstyleDao astyleDao;
	
	@Override
	public void addAstyle(Astyle astyle) {
		// TODO Auto-generated method stub
		astyleDao.addAstyle(astyle);
	}

	@Override
	public List<Astyle> getAllAstyle() {
		// TODO Auto-generated method stub
		return astyleDao.getAllAstyle();
	}

	@Override
	public boolean delAstyle(Integer id) {
		// TODO Auto-generated method stub
		return astyleDao.delAstyle(id);
	}

	@Override
	public Astyle getAstyle(Integer id) {
		// TODO Auto-generated method stub
		return astyleDao.getAstyle(id);
	}
	
	@Override
	public Astyle getAstyleByName(String name) {
		// TODO Auto-generated method stub
		return astyleDao.getAstyleByName(name);
	}

	@Override
	public boolean updateAstyle(Astyle astyle) {
		// TODO Auto-generated method stub
		return astyleDao.updateAstyle(astyle);
	}

}
