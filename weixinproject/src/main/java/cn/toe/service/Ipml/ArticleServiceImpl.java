package cn.toe.service.Ipml;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.toe.bean.Article;
import cn.toe.dao.ArticleDao;
import cn.toe.service.ArticleService;

@Service
public class ArticleServiceImpl implements ArticleService {

	@Autowired
	private ArticleDao articleDao;
	
	@Override
	public void addArticle(Article article) {
		// TODO Auto-generated method stub
		articleDao.addArticle(article);
	}

	@Override
	public List<Article> getAllArticle() {
		// TODO Auto-generated method stub
		return articleDao.getAllArticle();
	}

	@Override
	public boolean delArticle(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Article getArticle(Integer id) {
		// TODO Auto-generated method stub
		return articleDao.getArticle(id);
	}

	@Override
	public boolean updateArticle(Article article) {
		// TODO Auto-generated method stub
		return articleDao.updateArticle(article);
	}

}
