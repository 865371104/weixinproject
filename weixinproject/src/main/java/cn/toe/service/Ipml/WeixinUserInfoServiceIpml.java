package cn.toe.service.Ipml;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.toe.bean.WeixinUserInfo;
import cn.toe.dao.WeixinUserInfoDao;
import cn.toe.service.WeixinUserInfoService;

@Service
public class WeixinUserInfoServiceIpml implements WeixinUserInfoService {

	@Autowired
	private WeixinUserInfoDao weixinUserInfoDao;
	
	@Override
	public void addWeixinUserInfo(WeixinUserInfo weixinUserInfo) {
		// TODO Auto-generated method stub
		weixinUserInfoDao.addWeixinUserInfo(weixinUserInfo);
	}

	@Override
	public List<WeixinUserInfo> getAllWeixinUserInfo() {
		// TODO Auto-generated method stub
		return weixinUserInfoDao.getAllWeixinUserInfo();
	}

	@Override
	public boolean delWeixinUserInfo(Integer id) {
		// TODO Auto-generated method stub
		return weixinUserInfoDao.delWeixinUserInfo(id);
	}

	@Override
	public WeixinUserInfo getWeixinUserInfo(Integer id) {
		// TODO Auto-generated method stub
		return weixinUserInfoDao.getWeixinUserInfo(id);
	}

	@Override
	public WeixinUserInfo getWeixinUserInfoByOpenId(String openid) {
		// TODO Auto-generated method stub
		return weixinUserInfoDao.getWeixinUserInfoByOpenId(openid);
	}
	
	@Override
	public boolean updateWeixinUserInfo(WeixinUserInfo weixinUserInfo) {
		// TODO Auto-generated method stub
		return weixinUserInfoDao.updateWeixinUserInfo(weixinUserInfo);
	}
	

}
