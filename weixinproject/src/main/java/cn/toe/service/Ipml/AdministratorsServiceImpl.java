package cn.toe.service.Ipml;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.toe.bean.Administrators;
import cn.toe.dao.AdministratorsDao;
import cn.toe.service.AdministratorsService;

@Service
public class AdministratorsServiceImpl implements AdministratorsService {

	@Autowired
	private AdministratorsDao administratorsDao;
	
	@Override
	public void addAdministrators(Administrators administrators) {
		// TODO Auto-generated method stub
		administratorsDao.addAdministrators(administrators);
	}

	@Override
	public List<Administrators> getAllAdministrators() {
		// TODO Auto-generated method stub
		return administratorsDao.getAllAdministrators();
	}

	@Override
	public boolean delAdministrators(Integer id) {
		// TODO Auto-generated method stub
		return administratorsDao.delAdministrators(id);
	}

	@Override
	public Administrators getAdministrators(Integer id) {
		// TODO Auto-generated method stub
		return administratorsDao.getAdministrators(id);
	}

	@Override
	public Administrators getAdministratorsByName(String name) {
		// TODO Auto-generated method stub
		return administratorsDao.getAdministratorsByName(name);
	}
	
	@Override
	public boolean updateAdministrators(Administrators administrators) {
		// TODO Auto-generated method stub
		return administratorsDao.updateAdministrators(administrators);
	}  
	
}
