package cn.toe.service.Ipml;

import java.util.List;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.toe.bean.Authority;
import cn.toe.dao.AuthorityDao;
import cn.toe.service.AuthorityService;

@Service
public class AuthorityServiceIpml implements AuthorityService {

	@Autowired
	private AuthorityDao authorityDao;
	
	@Override
	public void addAuthority(Authority authority) {
		// TODO Auto-generated method stub
		authorityDao.addAuthority(authority);
	}

	@Override
	public List<Authority> getAllAuthority() {
		// TODO Auto-generated method stub
		return authorityDao.getAllAuthority();
	}

	@Override
	public List<Authority> getAllAuthoritybyNull(){
		// TODO Auto-generated method stub
		return authorityDao.getAllAuthoritybyNull();
		
	}
	
	@Override
	public List<Authority> getAllAuthoritybyNoNull(){
		// TODO Auto-generated method stub
		return authorityDao.getAllAuthoritybyNoNull();
		
	}
	
	@Override
	public boolean delAuthority(Integer id) {
		// TODO Auto-generated method stub
		return authorityDao.delAuthority(id);
	}

	@Override
	public Authority getAuthority(Integer id) {
		// TODO Auto-generated method stub
		return authorityDao.getAuthority(id);
	}

	@Override
	public boolean updateAuthority(Authority authority) {
		// TODO Auto-generated method stub
		return authorityDao.updateAuthority(authority);
	}

}
