package cn.toe.service;

import java.util.List;

import cn.toe.bean.Role;


public interface RoleService {  
	
	public void addRole(Role role);  
	    
	public List<Role> getAllRole();  
	    
	public boolean delRole(Integer id);  
	  
	public Role getRole(Integer id);  
	
	public Role getRoleByName(String name);
	 
	public boolean updateRole(Role role);

	
}