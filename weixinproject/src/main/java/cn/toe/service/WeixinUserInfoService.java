package cn.toe.service;

import java.util.List;

import cn.toe.bean.WeixinUserInfo;


public interface WeixinUserInfoService {
	public void addWeixinUserInfo(WeixinUserInfo weixinUserInfo);  
    
	public List<WeixinUserInfo> getAllWeixinUserInfo();  
	    
	public boolean delWeixinUserInfo(Integer id);  
	  
	public WeixinUserInfo getWeixinUserInfo(Integer id);  
	
	public WeixinUserInfo getWeixinUserInfoByOpenId(String openid);  
	 
	public boolean updateWeixinUserInfo(WeixinUserInfo weixinUserInfo);

	
}
