package cn.toe.service;

import java.util.List;

import cn.toe.bean.Article;

public interface ArticleService {
	public void addArticle(Article article);  
    
	public List<Article> getAllArticle();  
	    
	public boolean delArticle(Integer id);  
	  
	public Article getArticle(Integer id);  
	 
	public boolean updateArticle(Article article);
}
