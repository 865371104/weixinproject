package cn.toe.service;

import java.util.List;

import cn.toe.bean.Association;


public interface AssociationService {
	public void addAssociation(Association association);  
    
	public List<Association> getAllAssociation();  
	    
	public boolean delAssociation(String id);  
	  
	public Association getAssociation(String id);  
	 
	public boolean updateAssociation(Association association);
}
