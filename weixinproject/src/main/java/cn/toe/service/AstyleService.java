package cn.toe.service;

import java.util.List;

import cn.toe.bean.Astyle;

public interface AstyleService {
	public void addAstyle(Astyle astyle);  
    
	public List<Astyle> getAllAstyle();  
	    
	public boolean delAstyle(Integer id);  
	  
	public Astyle getAstyle(Integer id);  
	
	public Astyle getAstyleByName(String name);  
	 
	public boolean updateAstyle(Astyle astyle);
}
