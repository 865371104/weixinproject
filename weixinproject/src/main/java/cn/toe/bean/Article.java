package cn.toe.bean;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="Article")
public class Article implements Serializable {
	
	private static final long serialVersionUID = 2588199709884487540L;
	
	//文章编号
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ArticleId",insertable = true,updatable = true,nullable = false)
	private Integer articleId;
	
	@Column(name = "Title",nullable = false,length = 40)
	private String title;
	
	//文章内容
	@Column(name = "ArticleContent",length = 100)
	private String articleContent;
	
	//发布组织
	@Column(name = "Institutions",length = 20)
	private String institutions;
	
	//编辑作者
	@Column(name = "Editor",length = 10)
	private String editor;
	
	//文章相关图片
	@Column(name = "Img",length = 255)
	private String img;
	
	//备注
	@Column(name = "Note",length = 255)
	private String note;
	
	//相关行星
	@ManyToMany
	@JoinTable(name="t_article_planet", joinColumns={ @JoinColumn(name="article_id")}, 
    inverseJoinColumns={ @JoinColumn(name = "planetid") })
	private Set<Planet> planets;
	
	//文章类型
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name="Astyle")
	private Astyle astyle;

	public Integer getArticleId() {
		return articleId;
	}

	public void setArticleId(Integer articleId) {
		this.articleId = articleId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArticleContent() {
		return articleContent;
	}

	public void setArticleContent(String articleContent) {
		this.articleContent = articleContent;
	}

	public String getInstitutions() {
		return institutions;
	}

	public void setInstitutions(String institutions) {
		this.institutions = institutions;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Set<Planet> getPlanet() {
		return planets;
	}

	public void setPlanet(Set<Planet> planet) {
		this.planets = planet;
	}

	public Astyle getAstyle() {
		return astyle;
	}

	public void setAstyle(Astyle astyle) {
		this.astyle = astyle;
	}
	
	public Set<Planet> getPlanets() {
		return planets;
	}

	public void setPlanets(Set<Planet> planets) {
		this.planets = planets;
	}

	

	public Article(String title,String articleContent, String institutions, String editor, String img, String note,
			Set<Planet> planets, Astyle astyle) {
		this.title = title;
		this.articleContent = articleContent;
		this.institutions = institutions;
		this.editor = editor;
		this.img = img;
		this.note = note;
		this.planets = planets;
		this.astyle = astyle;
	}

	public Article() {
		// TODO Auto-generated constructor stub
		planets = new HashSet<Planet>();
	}
	
	 public void addPlanet(Planet planet)
	 {
		 planets.add(planet);
	 }
}
