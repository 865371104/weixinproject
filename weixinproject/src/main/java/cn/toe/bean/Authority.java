package cn.toe.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="authority")
public class Authority implements Serializable {

	private static final long serialVersionUID = 2588199709884487540L;
	
	@Id
	@Column(name = "authorityid",insertable = false,updatable = false,nullable = false,length=3)
	private int authorityId;
	
	@Column(name = "authorityname",insertable = true,nullable = false)
	private String authorityName;

	@Column(name = "authoritycontent")
	private String authorityContent;
	
	@ManyToMany(mappedBy="authoritys")
	private List<Role> roles;

	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name="authorityparent")
	private Authority authorityparent;
	

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "authorityparent")
	@LazyCollection(LazyCollectionOption.EXTRA)
	private List<Authority> Authoritys;
	
	
	
	public int getAuthorityId() {
		return authorityId;
	}

	public void setAuthorityId(int authorityId) {
		this.authorityId = authorityId;
	}

	public String getAuthorityName() {
		return authorityName;
	}

	public void setAuthorityName(String authorityName) {
		this.authorityName = authorityName;
	}

	public String getAuthorityContent() {
		return authorityContent;
	}

	public void setAuthorityContent(String authorityContent) {
		this.authorityContent = authorityContent;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Authority getAuthorityparent() {
		return authorityparent;
	}

	public void setAuthorityparent(Authority authorityparent) {
		this.authorityparent = authorityparent;
	}

	public List<Authority> getAuthoritys() {
		return Authoritys;
	}

	public void setAuthoritys(List<Authority> authoritys) {
		Authoritys = authoritys;
	}

	
	
	public Authority(String authorityName, String authorityContent) {
		this.authorityName = authorityName;
		this.authorityContent = authorityContent;
		roles = new ArrayList<Role>();
		Authoritys = new ArrayList<Authority>();
	}

	public Authority() {
		// TODO Auto-generated constructor stub
		roles = new ArrayList<Role>();
		Authoritys = new ArrayList<Authority>();
	}

	public void addRole(Role role){
		roles.add(role);
	}
	
	public void addAuthority(Authority authority){
		Authoritys.add(authority);
	}
}
