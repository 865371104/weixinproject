package cn.toe.bean;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

//管理员表
@Entity
@Table(name="administrators")
public class Administrators implements Serializable {
	
	private static final long serialVersionUID = 2588199709884487540L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AdminId",insertable = true,updatable = true,nullable = false)
	private Integer adminid;

    @ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="role")
	private Role role;
	
	@Column(name = "Adname",length=20)
	private String adname;
	
	@Column(name = "PassWord",length=20)
	private String passWord;
	
	@Column(name = "DocumentType",length=10)
	private String documentType;
	
	@Column(name = "DocumentId",length=30)
	private String documentId;
	
	@Column(name = "Email",length=20)
	private String email;
	
	@Column(name = "Tphone",length=20)
	private String tphone;
	
	@OneToOne(mappedBy = "adminid")
	private WeixinUserInfo WeixinUserInfo;
	
	public Integer getAdminid() {
		return adminid;
	}

	public void setAdminid(Integer adminid) {
		this.adminid = adminid;
	}
	
	@OneToOne(mappedBy="Role")
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getAdname() {
		return adname;
	}

	public void setAdname(String adname) {
		this.adname = adname;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTphone() {
		return tphone;
	}

	public void setTphone(String tphone) {
		this.tphone = tphone;
	}
	
	public WeixinUserInfo getWeixinUserInfo() {
		return WeixinUserInfo;
	}

	public void setWeixinUserInfo(WeixinUserInfo weixinUserInfo) {
		WeixinUserInfo = weixinUserInfo;
	}

	public Administrators(Integer adminid, String adname, String passWord, String documentType,
			String documentId, String email, String tphone) {
		this.adminid = adminid;
		this.adname = adname;
		this.passWord = passWord;
		this.documentType = documentType;
		this.documentId = documentId;
		this.email = email;
		this.tphone = tphone;
	}

	public Administrators() {
		// TODO Auto-generated constructor stub
	}
	
	
}
