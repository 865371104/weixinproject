package cn.toe.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.beans.factory.annotation.Autowired;

import cn.toe.dao.RoleDao;
import cn.toe.dao.Impl.RoleDaoImpl;
import cn.toe.service.RoleService;
import cn.toe.service.Ipml.RoleServiceImpl;

//用户表
@Entity
@Table(name="weixinUserInfo")
public class WeixinUserInfo implements Serializable {
	
	private static final long serialVersionUID = 2588199709884487540L;
	
	//数据库编号
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "userId",insertable = true,updatable = true,nullable = false)
	private Integer userId;
	
	//用户的标识
	@Column(name = "openId",nullable = false)
	private String openId;
	
	// 关注状态（1是关注，0是未关注），未关注时获取不到其余信息
	@Column(name = "subscribe",nullable = false)
	private Integer subscribe;

	//用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
	@Column(name = "subscribeTime",nullable = false)
	private String subscribeTime;

	//昵称
	@Column(name = "nickname")
	private String nickName;
	
	// 用户的性别（1是男性，2是女性，0是未知）
	@Column(name = "sex")
	private Integer sex;

	// 用户所在国家
	@Column(name = "country")
	private String country;
	
	// 用户所在省份
	@Column(name = "province")
	private String province;
	
	// 用户所在城市
	@Column(name = "city")
	private String city;
	
	// 用户的语言，简体中文为zh_CN
	@Column(name = "language")
	private String language;
	
	// 用户头像
	@Column(name = "headImgUrl")
	private String headImgUrl;
	
	//管理相关
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="adminid")   //这里指定的是数据库中的外键字段。
	private Administrators adminid;
	
	//关注的行星
	@ManyToMany
	@JoinTable(name="t_follow_planet", joinColumns={ @JoinColumn(name="User_id")}, 
    inverseJoinColumns={ @JoinColumn(name = "planetid") })
	private Set<Planet> planets;
	
	//关注的用户
	@ManyToMany
	@JoinTable(name="t_follow_users", joinColumns={ @JoinColumn(name="User_id")}, 
    inverseJoinColumns={ @JoinColumn(name = "Users_id") })
	private Set<WeixinUserInfo> users;
	
	@ManyToMany
	@JoinTable(name="t_follow_astyle", joinColumns={ @JoinColumn(name="User_id")}, 
    inverseJoinColumns={ @JoinColumn(name = "Astyle_id") })
	private Set<Astyle> astyle;
	
	
	@ManyToMany
	@JoinTable(name="t_follow_galaxy", joinColumns={ @JoinColumn(name="User_id")}, 
    inverseJoinColumns={ @JoinColumn(name = "Galaxyid") })
	private Set<Galaxy> galaxy;
	
		

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public Integer getSubscribe() {
		return subscribe;
	}

	public void setSubscribe(Integer subscribe) {
		this.subscribe = subscribe;
	}

	public String getSubscribeTime() {
		return subscribeTime;
	}

	public void setSubscribeTime(String subscribeTime) {
		this.subscribeTime = subscribeTime;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public Administrators getAdminid() {
		return adminid;
	}

	public void setAdminid(Administrators adminid) {
		this.adminid = adminid;
	}

	public Set<Planet> getPlanets() {
		return planets;
	}

	public void setPlanets(Set<Planet> planets) {
		this.planets = planets;
	}

	public Set<WeixinUserInfo> getUsers() {
		return users;
	}

	public void setUsers(Set<WeixinUserInfo> users) {
		this.users = users;
	}

	public Set<Astyle> getAstyle() {
		return astyle;
	}

	public void setAstyle(Set<Astyle> astyle) {
		this.astyle = astyle;
	}

	public Set<Galaxy> getGalaxy() {
		return galaxy;
	}

	public void setGalaxy(Set<Galaxy> galaxy) {
		this.galaxy = galaxy;
	}

	
	
	public WeixinUserInfo(String openId, Integer subscribe, String subscribeTime, String nickName, Integer sex, String country,
			String province, String city, String language, String headImgUrl) {
		this.openId = openId;
		this.subscribe = subscribe;
		this.subscribeTime = subscribeTime;
		this.nickName = nickName;
		this.sex = sex;
		this.country = country;
		this.province = province;
		this.city = city;
		this.language = language;
		this.headImgUrl = headImgUrl;
		planets = new HashSet<Planet>();
		users = new HashSet<WeixinUserInfo>();
		astyle = new HashSet<Astyle>();
		galaxy = new HashSet<Galaxy>();
	}

	public WeixinUserInfo() {
		// TODO Auto-generated constructor stub
		planets = new HashSet<Planet>();
		users = new HashSet<WeixinUserInfo>();
		astyle = new HashSet<Astyle>();
		galaxy = new HashSet<Galaxy>();
		
	}

	public void addPlanet(Planet planet)
	{
		planets.add(planet);
	}
	
	public void addUsers(WeixinUserInfo user){
		users.add(user);
	}
	
	public void addAstyle(Astyle Astyle){
		astyle.add(Astyle);
	}
	
	public void addGalaxy(Galaxy Galaxy){
		galaxy.add(Galaxy);
	}
	
	
}
