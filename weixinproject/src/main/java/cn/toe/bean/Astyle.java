package cn.toe.bean;
//类型表

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="astyle")
public class Astyle implements Serializable {
	
	private static final long serialVersionUID = 2588199709884487540L;
	
	//标号
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AstyleId",insertable = true,updatable = true,nullable = false)
	private Integer astyleId;
	
	@Column(name = "Sname",nullable = false,length = 20)
	private String sname;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "astyle")
	@LazyCollection(LazyCollectionOption.EXTRA)
	private List<Article> articles;
	
	@ManyToMany(mappedBy="astyle")
	private List<WeixinUserInfo> weixinUserInfos;

	public Integer getAstyleId() {
		return astyleId;
	}

	public void setAstyleId(Integer astyleId) {
		this.astyleId = astyleId;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public List<WeixinUserInfo> getWeixinUserInfos() {
		return weixinUserInfos;
	}

	public void setWeixinUserInfos(List<WeixinUserInfo> weixinUserInfos) {
		this.weixinUserInfos = weixinUserInfos;
	}

	public Astyle(String sname) {
		this.sname = sname;
		articles = new ArrayList<Article>();
		weixinUserInfos = new ArrayList<WeixinUserInfo>();
	}

	public Astyle() {
		// TODO Auto-generated constructor stub
		articles = new ArrayList<Article>();
		weixinUserInfos = new ArrayList<WeixinUserInfo>();
	}

	public void addArticle(Article article)
	{
		articles.add(article);
	}

	public void addPlanet(WeixinUserInfo weixinUserInfo)
	{
		weixinUserInfos.add(weixinUserInfo);
	}
}
