package cn.toe.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


@Entity
@Table(name="role")
public class Role implements Serializable {

	private static final long serialVersionUID = 2588199709884487540L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Role",nullable = false,length=3)
	private Integer role;
	
	@Column(name = "Content",nullable = false)
	private String content;

	@OneToMany(cascade = CascadeType.ALL,mappedBy = "role")
	@LazyCollection(LazyCollectionOption.EXTRA)
	private List<Administrators> administratorss;
	
	@ManyToMany
	@JoinTable(name="t_role_authority", joinColumns={ @JoinColumn(name="role")}, 
	inverseJoinColumns={ @JoinColumn(name = "authorityid") })
	private List<Authority> authoritys;

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<Administrators> getAdministratorss() {
		return administratorss;
	}

	public void setAdministratorss(List<Administrators> administratorss) {
		this.administratorss = administratorss;
	}

	public List<Authority> getAuthoritys() {
		return authoritys;
	}

	public void setAuthoritys(List<Authority> authoritys) {
		this.authoritys = authoritys;
	}

	public Role(Integer role, String content) {
		this.role = role;
		this.content = content;
		authoritys = new ArrayList<Authority>();
		administratorss = new ArrayList<Administrators>();
	}

	public Role() {
		// TODO Auto-generated constructor stub
		authoritys = new ArrayList<Authority>();
		administratorss = new ArrayList<Administrators>();
	}
	
	public void addAuthority(Authority authority){
		authoritys.add(authority);
	}
	
	public void addAdministratorss(Administrators administrators){
		administratorss.add(administrators);
	}
	
	public void delAdministratorss(Administrators administrators){
		administratorss.remove(administrators);
	}
}
