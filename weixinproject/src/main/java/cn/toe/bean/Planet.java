package cn.toe.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="planet")
public class Planet implements Serializable {
	
	private static final long serialVersionUID = 2588199709884487540L;
	
	//数据库编号
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Planetid",insertable = true,updatable = true,nullable = false)
	private Integer planetid;
	
	//行星name
	@Column(name = "Planename",length = 20)
	private String planename;
	
	//主星编号
	@Column(name = "Ruler")
	private Integer ruler;
	
	//半径
	@Column(name = "Radius")
	private Double radius;
	
	//半径
	@Column(name = "Quality")
	private Double quality;
	
	//轨道周期
	@Column(name = "Cycle")	
	private Double cycle;
	
	//轨道半长轴
	@Column(name = "Sma")		
	private Double sma;
	
	//轨道离心率
	@Column(name = "Cro")	
	private Double cro;
	
	//轨道倾角
	@Column(name = "Dipangle")	
	private Double dipangle;
	
	//发现方法
	@Column(name = "Dm",length = 40)		
	private String dm;
	
	//发现年份
	@Column(name = "Date")	
	private Date date;
	
	//距离
	@Column(name = "Distance")	
	private Double distance;
	
	//所属星系
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name="galaxy")
	private Galaxy galaxy;
	
	//官方编号
	@Column(name = "Numvar",length = 40)		
	private String numvar;
	
	//图片地址
	@Column(name = "Img",length = 40)		
	private String img;
	
	//行星描述
	@Column(name = "Note",length = 255)		
	private String note;

	@ManyToMany(mappedBy="planets")
	private Set<Article> articles;
	
	@ManyToMany(mappedBy="planets")
	private Set<WeixinUserInfo> weixinUserInfos;
	
	public Integer getPlanetid() {
		return planetid;
	}

	public void setPlanetid(Integer planetid) {
		this.planetid = planetid;
	}

	public String getPlanename() {
		return planename;
	}

	public void setPlanename(String planename) {
		this.planename = planename;
	}

	public Integer getRuler() {
		return ruler;
	}

	public void setRuler(Integer ruler) {
		this.ruler = ruler;
	}

	public Double getRadius() {
		return radius;
	}

	public void setRadius(Double radius) {
		this.radius = radius;
	}

	public Double getQuality() {
		return quality;
	}

	public void setQuality(Double quality) {
		this.quality = quality;
	}

	public Double getCycle() {
		return cycle;
	}

	public void setCycle(Double cycle) {
		this.cycle = cycle;
	}

	public Double getSma() {
		return sma;
	}

	public void setSma(Double sma) {
		this.sma = sma;
	}

	public Double getCro() {
		return cro;
	}

	public void setCro(Double cro) {
		this.cro = cro;
	}

	public Double getDipangle() {
		return dipangle;
	}

	public void setDipangle(Double dipangle) {
		this.dipangle = dipangle;
	}

	public String getDm() {
		return dm;
	}

	public void setDm(String dm) {
		this.dm = dm;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public Galaxy getGalaxy() {
		return galaxy;
	}

	public void setGalaxy(Galaxy galaxy) {
		this.galaxy = galaxy;
	}

	public String getNumvar() {
		return numvar;
	}

	public void setNumvar(String numvar) {
		this.numvar = numvar;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Set<Article> getArticles() {
		return articles;
	}

	public void setArticles(Set<Article> articles) {
		this.articles = articles;
	}

	

	public Planet(String planename, Integer ruler, Double radius, Double quality, Double cycle, Double sma, Double cro,
			Double dipangle, String dm, Date date, Double distance, Galaxy galaxy, String numvar, String img,
			String note, Set<Article> articles) {
		this.planename = planename;
		this.ruler = ruler;
		this.radius = radius;
		this.quality = quality;
		this.cycle = cycle;
		this.sma = sma;
		this.cro = cro;
		this.dipangle = dipangle;
		this.dm = dm;
		this.date = date;
		this.distance = distance;
		this.galaxy = galaxy;
		this.numvar = numvar;
		this.img = img;
		this.note = note;
		this.articles = articles;
	}

	public Planet() {
		articles = new HashSet<Article>();
		weixinUserInfos = new HashSet<WeixinUserInfo>();
		// TODO Auto-generated constructor stub
	}
	
	public void addArticle(Article article)
	 {
		articles.add(article);
	 }

	public void addPlanet(WeixinUserInfo weixinUserInfo)
	 {
		weixinUserInfos.add(weixinUserInfo);
	 }
	
}
