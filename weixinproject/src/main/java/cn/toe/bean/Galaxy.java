package cn.toe.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="galaxy")
public class Galaxy implements Serializable {
	
	private static final long serialVersionUID = 2588199709884487540L;

	//星系编号
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Galaxyid",insertable = true,updatable = true,nullable = false)
	private Integer galaxyid;
	
	//星系名称
	@Column(name = "GalaxyName")
	private String galaxyName;
	
	//官方星系编号
	@Column(name = "GalaxyIN")	
	private String galaxyIN;

	//所属大星系
	@Column(name = "GalaxyNum")		
	private String galaxyNum;
	
	//类型
	@Column(name = "Type")			
	private String type;
	
	//图片地址
	@Column(name = "Img")	
	private String img;
	
	//发现日期
	@Column(name = "Fyear")	
	private Date fyear;
	
	//备注
	@Column(name = "Note")	
	private String note;
	
	//主星编号
	@OneToOne
	@JoinColumn(name="Ruler")		
	private Planet ruler;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "galaxy")
	@LazyCollection(LazyCollectionOption.EXTRA)
	private Set<Planet> planets;

	@ManyToMany(mappedBy="galaxy")
	private Set<WeixinUserInfo> weixinUserInfos;
	
	public Integer getGalaxyid() {
		return galaxyid;
	}

	public void setGalaxyid(Integer galaxyid) {
		this.galaxyid = galaxyid;
	}

	public String getGalaxyName() {
		return galaxyName;
	}

	public void setGalaxyName(String galaxyName) {
		this.galaxyName = galaxyName;
	}

	public String getGalaxyIN() {
		return galaxyIN;
	}

	public void setGalaxyIN(String galaxyIN) {
		this.galaxyIN = galaxyIN;
	}

	public String getGalaxyNum() {
		return galaxyNum;
	}

	public void setGalaxyNum(String galaxyNum) {
		this.galaxyNum = galaxyNum;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Date getFyear() {
		return fyear;
	}

	public void setFyear(Date fyear) {
		this.fyear = fyear;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Planet getRuler() {
		return ruler;
	}

	public void setRuler(Planet ruler) {
		this.ruler = ruler;
	}

	public Set<Planet> getPlanet() {
		return planets;
	}

	public void setPlanet(Set<Planet> planet) {
		this.planets = planet;
	}

	

	public Galaxy(String galaxyName, String galaxyIN, String galaxyNum, String type, String img, Date fyear,
			String note, Planet ruler, Set<Planet> planets) {
		this.galaxyName = galaxyName;
		this.galaxyIN = galaxyIN;
		this.galaxyNum = galaxyNum;
		this.type = type;
		this.img = img;
		this.fyear = fyear;
		this.note = note;
		this.ruler = ruler;
		this.planets = planets;
	}

	public Galaxy() {
		// TODO Auto-generated constructor stub
		planets = new HashSet<Planet>();
		weixinUserInfos = new HashSet<WeixinUserInfo>();
	}
	
	public void addPlanet(Planet planet)
	{
		planets.add(planet);
	}

	public void addPlanet(WeixinUserInfo weixinUserInfo)
	{
		weixinUserInfos.add(weixinUserInfo);
	}
	
}
