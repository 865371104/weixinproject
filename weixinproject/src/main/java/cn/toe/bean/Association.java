package cn.toe.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
//协会表
@Entity
@Table(name="association")
public class Association implements Serializable {
	
	private static final long serialVersionUID = 2588199709884487540L;
	//协会编号
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AssociationId",insertable = true,updatable = true,nullable = false)
	private Integer associationId;
	
	//协会名
	@Column(name = "Aname")
	private String aname;
	
	//官网
	@Column(name = "Url")
	private String url;
	
	//类型
	@Column(name = "Type")
	private String type;
	
	//地址
	@Column(name = "Address")
	private String address;
	
	//联系电话
	@Column(name = "Phone")
	private String phone;
	
	//备注
	@Column(name = "Note")
	private String note;
	
	//创建日期
	@Column(name = "Cdate")
	private Date cdate;
	
	//法人
	@Column(name = "Ceo")
	private String ceo;

	public Integer getAssociationId() {
		return associationId;
	}

	public void setAssociationId(Integer associationId) {
		this.associationId = associationId;
	}

	public String getAname() {
		return aname;
	}

	public void setAname(String aname) {
		this.aname = aname;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getCdate() {
		return cdate;
	}

	public void setCdate(Date cdate) {
		this.cdate = cdate;
	}

	public String getCeo() {
		return ceo;
	}

	public void setCeo(String ceo) {
		this.ceo = ceo;
	}

	public Association(Integer associationId, String aname, String url, String type, String address, String phone,
			String note, Date cdate, String ceo) {
		this.associationId = associationId;
		this.aname = aname;
		this.url = url;
		this.type = type;
		this.address = address;
		this.phone = phone;
		this.note = note;
		this.cdate = cdate;
		this.ceo = ceo;
	}

	public Association() {
		// TODO Auto-generated constructor stub
	}
	
	
	
}
