package cn.toe.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.toe.bean.Administrators;
import cn.toe.bean.Authority;
import cn.toe.bean.Role;
import cn.toe.service.AdministratorsService;
import cn.toe.service.AuthorityService;
import cn.toe.service.RoleService;
import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;

@Controller
@RequestMapping("/role")
public class RoleController {
 
	private String contentname = "";
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private AdministratorsService administratorsService;
	
	@Autowired
	private AuthorityService authorityService;
	
	@RequestMapping("/getAllRole")
	public String getAllGalaxy(HttpServletRequest request){
		List<Role> roles = roleService.getAllRole();
		request.setAttribute("roles", roles);
		return "jsp/Role/RoleManager";
	}
	
	@RequestMapping("/toAllRoleList")
	private void toAllRoleList(String page,String rows,HttpServletResponse response){
		List<Role> roles = roleService.getAllRole();
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		JsonConfig config = new JsonConfig();
		config.setExcludes(new String[]{"administratorss","authority","authoritys"});
		JSONArray json = JSONArray.fromObject(roles, config);
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(json.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/toAdministrators")
	public String toAdministrators(HttpServletRequest request,Integer id){
		Role role = roleService.getRole(id);
		List<Administrators> administratorss = role.getAdministratorss();
		request.setAttribute("administratorss", administratorss);
		return "jsp/Role/AdminsManager";
	}
	@RequestMapping("/toAuthority")
	public String toAuthority(HttpServletRequest request,Integer id){
		Role role = roleService.getRole(id);
		List<Authority> Authority = role.getAuthoritys();
		List<Authority> AllParentAuthority = authorityService.getAllAuthoritybyNull();
		List<Authority> AllAuthority = authorityService.getAllAuthoritybyNoNull();
		request.setAttribute("Authority", Authority);
		request.setAttribute("AllParentAuthority", AllParentAuthority);
		request.setAttribute("AllAuthority", AllAuthority);
		return "jsp/Role/AuthorityManager";
	}
	
	@ResponseBody
	@RequestMapping(value = "/checkContent", method = RequestMethod.POST)
	public String checkContent(HttpServletRequest request,String name) {
	    boolean isOk = false;
	    if(name.equals(this.contentname)&&this.contentname!=""){
			isOk = true;
		}
	    return isOk+"";
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/check", method = RequestMethod.POST)
	public void check(HttpServletRequest request,String name) {
	    if(roleService.getRoleByName(name)!=null){
	    	Role role1 = roleService.getRoleByName(name);
	    	this.contentname = role1.getContent();
	    }else{
	    	this.contentname = "没有找到";
	    }
	}
	
	@ResponseBody
	@RequestMapping("/delRole")
	public void delRole(Integer id,HttpServletResponse response){
		boolean i = false;
		try {
			Role role = roleService.getRole(id);
			List<Authority> Authority = role.getAuthoritys();
			Authority.clear();
			Role role2 = roleService.getRole(2);
			List<Administrators> admins = role.getAdministratorss();
			for(int j = 0;j < admins.size();j++){
				Administrators user = admins.get(j) ;
				user.setRole(role2);
				administratorsService.updateAdministrators(user);
			}
			admins.clear();
			roleService.updateRole(role);
			i = roleService.delRole(id);
		} catch (Exception e) {
			// TODO: handle exception
		}
		String result = "{\"result\":\"error\"}";
		if(i){
			result = "{\"result\":\"success\"}";
		}
		PrintWriter out = null;
		response.setContentType("application/json");
		
		try {
			out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	@ResponseBody
	@RequestMapping(value = "/outcheck", method = RequestMethod.POST)
	public void outcheck(HttpServletRequest request) {
	    this.contentname = "";
	}
	
	@RequestMapping("/toRenameRole")
	private String toRenameRole(HttpServletRequest request,Integer id){
		Role role = roleService.getRole(id);
		request.setAttribute("role", role);
		return "jsp/Role/ReRoleName";
	}
	
	@RequestMapping("/RenameRole")
	private String RenameRole(HttpServletRequest request,Role role){
		HttpSession mySession = request.getSession();
		Integer id = role.getRole();
		Role roles = roleService.getRole(id);
		roles.setContent(role.getContent());
		try {
			if(roleService.updateRole(roles)){
				mySession.setAttribute("updateroleContent", "修改信息成功");
			}else{
				mySession.setAttribute("updateroleContent", "修改信息失败");
			}
		} catch (Exception e) {
			// TODO: handle exception
			mySession.setAttribute("updateroleContent", "修改信息失败");
		}
		return "jsp/Role/ReRoleName";
	}
	
	@RequestMapping("/toReAdminRole")
	private String toReAdminRole(HttpServletRequest request,Integer id){
		Administrators admin = administratorsService.getAdministrators(id);
		List<Role> roles = roleService.getAllRole();
		request.setAttribute("roles", roles);
		request.setAttribute("admin", admin);
		return "jsp/Role/ReAdminRole";
	}
	
	@RequestMapping("/ReAdminRole")
	private String ReAdminRole(HttpServletRequest request,Integer role,Integer admin){
		HttpSession mySession = request.getSession();
		Administrators administrators = administratorsService.getAdministrators(admin);
		Role adminrole = roleService.getRole(role);
		administrators.setRole(adminrole);
		try {
			if(administratorsService.updateAdministrators(administrators)){
				mySession.setAttribute("updateadminrole", "修改权限成功");
			}else{
				mySession.setAttribute("updateadminrole", "修改权限失败");
			}
		} catch (Exception e) {
			// TODO: handle exception
			mySession.setAttribute("updateadminrole", "修改权限失败");
		}
		return "jsp/Role/ReAdminRole";
	}
	
	@ResponseBody
	@RequestMapping("/delAdminRole")
	private void delAdminRole(Integer id,HttpServletResponse response){
		boolean i = false;
		
		Administrators administrators = administratorsService.getAdministrators(id);
		Role adminrole = roleService.getRole(2);
		administrators.setRole(adminrole);
		i = administratorsService.updateAdministrators(administrators);
		String result = "{\"result\":\"error\"}";
		if(i){
			result = "{\"result\":\"success\"}";
		}
		PrintWriter out = null;
		response.setContentType("application/json");
		
		try {
			out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	@RequestMapping("/addRole")
	private String addRole(Role role){
		roleService.addRole(role);
		return "jsp/Role/addRole";
	}
	
	@RequestMapping("/toaddRole")
	private String toaddRole(){
		return "jsp/Role/addRole";
	}
}
