package cn.toe.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.toe.bean.Administrators;
import cn.toe.bean.Authority;
import cn.toe.bean.Role;
import cn.toe.bean.WeixinUserInfo;
import cn.toe.service.AdministratorsService;
import cn.toe.service.WeixinUserInfoService;
import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;

@Controller
@RequestMapping("/Admin")
public class AdminController {
	
	
	private String AdminOpenid = "";
	
	private String AdminOpenid2 = "";
	
	private String Adminsname = "";
	
	@Autowired
	private AdministratorsService administratorsService;
	
	@Autowired
	private WeixinUserInfoService weixinUserInfoService;
	
	@RequestMapping("/toAllAdminMsg")
	private String toAllAdminMsg(HttpServletRequest request,HttpServletResponse response){
		HttpSession mySession = request.getSession();
		List<Administrators> Admins = administratorsService.getAllAdministrators();
		return "/jsp/Administrators/AdminMessage";
	}
	
	@RequestMapping("/toAllAdminMsgList")
	private void toAllAdminMsgList(String page,String rows,HttpServletResponse response){
		List<Administrators> Admins = administratorsService.getAllAdministrators();
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		JsonConfig config = new JsonConfig();
		config.setExcludes(new String[]{"weixinUserInfo","role"});
		System.out.println(JSONArray.fromObject(Admins, config));
		JSONArray json = JSONArray.fromObject(Admins, config);
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(json.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@ResponseBody
	@RequestMapping("/delAdmin")
	public void delAdmin(Integer id,HttpServletResponse response){
		boolean i = false;
		if(id!=1){
			try {
				Administrators admin = administratorsService.getAdministrators(id);
				admin.setRole(null);
				admin.setWeixinUserInfo(null);
				administratorsService.updateAdministrators(admin);
				i = administratorsService.delAdministrators(id);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}	
		String result = "{\"result\":\"error\"}";
		if(i){
			result = "{\"result\":\"success\"}";
		}
		PrintWriter out = null;
		response.setContentType("application/json");
		
		try {
			out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	@RequestMapping("/addAdmin")
	private String addAdmin(String openid,String adminname,String pwd){
		Administrators admin = new Administrators();
		admin.setAdname(adminname);
		if(openid !=""){
			WeixinUserInfo weixiner = weixinUserInfoService.getWeixinUserInfoByOpenId(openid);
			admin.setWeixinUserInfo(weixiner);
		}
		if(pwd !=""){
			admin.setPassWord(pwd);
		}
		administratorsService.addAdministrators(admin);
		return "jsp/Administrators/addAdmin";
	}
	
	@RequestMapping("/toAddAdmin")
	private String toAddAdmin(){
		return "jsp/Administrators/addAdmin";
	}
	
	@ResponseBody
	@RequestMapping(value = "/checkOpenIda", method = RequestMethod.POST)
	public String checkOpenIda(HttpServletRequest request,String name) {
	    boolean isOk = false;
	    if(name.equals(this.AdminOpenid)&&this.AdminOpenid!=""){
			isOk = true;
		}
	    return isOk+"";
	}
	
	@ResponseBody
	@RequestMapping(value = "/checkOpenIdb", method = RequestMethod.POST)
	public String checkOpenIdb(HttpServletRequest request,String name) {
	    boolean isOk = false;
	    if(name.equals(this.AdminOpenid2)&&this.AdminOpenid2!=""){
			isOk = true;
		}
	    return isOk+"";
	}
	
	@ResponseBody
	@RequestMapping(value = "/checkAdminname", method = RequestMethod.POST)
	public String checkAdminname(HttpServletRequest request,String name) {
	    boolean isOk = false;
	    if(name.equals(this.Adminsname)&&this.Adminsname!=""){
			isOk = true;
		}
	    return isOk+"";
	}
	
	@ResponseBody
	@RequestMapping(value = "/check", method = RequestMethod.POST)
	public void check(HttpServletRequest request,String name1,String name2) {
	    if(administratorsService.getAdministratorsByName(name2)!=null){
	    	this.Adminsname = administratorsService.getAdministratorsByName(name2).getAdname();
	    }else{
	    	this.Adminsname = "没有找到";
	    }
	    if(weixinUserInfoService.getWeixinUserInfoByOpenId(name1)==null){
	    	//由此用户未被注册
    		this.AdminOpenid = name1;
	    }else{
	    	this.AdminOpenid = "没有找到";
	    }
	    if(name1!=""&&weixinUserInfoService.getWeixinUserInfoByOpenId(name1).getAdminid()!=null){
	    	//由此用户被注册
    		this.AdminOpenid2 = name1;
    	}else{
    		this.AdminOpenid2 = "没有找到";
    	}
	}
	
	@ResponseBody
	@RequestMapping(value = "/outcheck", method = RequestMethod.POST)
	public void outcheck(HttpServletRequest request) {
	    this.AdminOpenid = "";
	    this.AdminOpenid2 = "";
	    this.Adminsname = "";
	}
}
