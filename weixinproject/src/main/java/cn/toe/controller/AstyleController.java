package cn.toe.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.toe.bean.Administrators;
import cn.toe.bean.Article;
import cn.toe.bean.Astyle;
import cn.toe.bean.Authority;
import cn.toe.bean.Role;
import cn.toe.bean.WeixinUserInfo;
import cn.toe.service.AstyleService;
import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;

@Controller
@RequestMapping("/astyle")
public class AstyleController {
	
	private String astylename="";
	
	@Autowired
	private AstyleService astyleService;
	
	@RequestMapping("/getAllAstyle")
	public void getAllAstyle(HttpServletRequest request,HttpServletResponse response){
		List<Astyle> astyles = astyleService.getAllAstyle();
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		JSONArray json = new JSONArray();  
		json.addAll(astyles);  
		json.listIterator();
		PrintWriter out = null;
		System.out.println(json);
		try {
			out = response.getWriter();
			out.write(json.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/toAllAstyle")
	public String getAllGalaxy(HttpServletRequest request){
		return "jsp/Article/astyleMessage";
	}
	
	@RequestMapping("/toAllAstyleList")
	public void toAllAstyleList(HttpServletRequest request,HttpServletResponse response){
		List<Astyle> astyles = astyleService.getAllAstyle();
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		JsonConfig config = new JsonConfig();
		config.setExcludes(new String[]{"weixinUserInfos","articles"});
		JSONArray json = JSONArray.fromObject(astyles, config);
		for(int i = 0;i<astyles.size();i++){
			List<WeixinUserInfo> weixinUserInfos = astyles.get(i).getWeixinUserInfos();
			List<Article> articles = astyles.get(i).getArticles();
			json.getJSONObject(i).put("weixinUserInfosSize", weixinUserInfos.size());
			json.getJSONObject(i).put("articlesSize", articles.size());
		}
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(json.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/toAddAstyle")
	public String toAddAstyle(){
		return "jsp/Article/addAstyle";
	}
	
	@RequestMapping("/addAstyle")
	public String addAstyle(Astyle astyle){
		astyleService.addAstyle(astyle);
		return "jsp/Article/addAstyle";
	}
	
	@RequestMapping("/toEditAstyle")
	public String toEditAstyle(){
		return "jsp/Article/editAstyle";
	}
	
	@RequestMapping("/editAstyle")
	public String editAstyle(Astyle astyle){
		astyleService.updateAstyle(astyle);
		return "jsp/Article/editAstyle";
	}
	
	@ResponseBody
	@RequestMapping(value = "/checkAstyle", method = RequestMethod.POST)
	public String checkAstyle(HttpServletRequest request,String name) {
	    boolean isOk = false;
	    if(name.equals(this.astylename)&&this.astylename!=""){
			isOk = true;
		}
	    return isOk+"";
	}
	
	@ResponseBody
	@RequestMapping(value = "/check", method = RequestMethod.POST)
	public void check(HttpServletRequest request,String name) {
	    if(astyleService.getAstyleByName(name)!=null){
	    	Astyle astyle = astyleService.getAstyleByName(name);
	    	this.astylename = astyle.getSname();
	    }else{
	    	this.astylename = "没有找到";
	    }
	}
	
	@ResponseBody
	@RequestMapping(value = "/outcheck", method = RequestMethod.POST)
	public void outcheck(HttpServletRequest request) {
	    this.astylename = "";
	}
	
	@ResponseBody
	@RequestMapping("/delAstyle")
	public void delAstyle(Integer id,HttpServletResponse response){
		boolean i = false;
		try {
			i = astyleService.delAstyle(id);
		} catch (Exception e) {
			// TODO: handle exception
		}
		String result = "{\"result\":\"error\"}";
		if(i){
			result = "{\"result\":\"success\"}";
		}
		PrintWriter out = null;
		response.setContentType("application/json");
		
		try {
			out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
}
