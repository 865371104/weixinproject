package cn.toe.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.toe.bean.Administrators;
import cn.toe.service.AdministratorsService;

@Controller
@RequestMapping("/loginer")
public class LoginerController {

	@Autowired
	private AdministratorsService administratorsService;
	
	private String uname = "";
	
	private String upwd = "";
	
	@ResponseBody
	@RequestMapping(value = "/checkPWD", method = RequestMethod.POST)
	public String checkPassWord(HttpServletRequest request,String username,String pwd) {
	    boolean isOk = false;
	    if(username.equals(this.uname)&&this.uname!=""){
			if(!pwd.equals(upwd)){
				isOk = true;
			}
		}
	    return isOk+"";
	}
	
	@ResponseBody
	@RequestMapping(value = "/checkUN", method = RequestMethod.POST)
	public String checkUsername(HttpServletRequest request,String username) {
	    boolean isOk = false;
	    if(!username.equals(this.uname)&&this.uname!=""){
	        isOk = true;
	    }
	    return isOk+"";
	}
	
	@ResponseBody
	@RequestMapping(value = "/check", method = RequestMethod.POST)
	public void check(HttpServletRequest request,String username,String pwd) {
		System.out.println("username");
	    if(administratorsService.getAdministratorsByName(username)!=null){
	    	Administrators administrators = administratorsService.getAdministratorsByName(username);
	    	this.uname = administrators.getAdname();
	    	this.upwd = administrators.getPassWord();
	    }else{
	    	this.uname = "没有找到";
	    	this.upwd = "没有找到";
	    }
	    System.out.println(this.uname);
	    System.out.println(this.upwd);
	}
	
	@ResponseBody
	@RequestMapping(value = "/outcheck", method = RequestMethod.POST)
	public void outcheck(HttpServletRequest request) {
	    this.uname = "";
	    this.upwd = "";
	    System.out.println("112");
	}
	
	@RequestMapping("/login")
	public String login(String username,String pwd,HttpServletRequest request){
		if(administratorsService.getAdministratorsByName(username)!=null){
			Administrators administrators = administratorsService.getAdministratorsByName(username);
			HttpSession mySession = request.getSession();
			mySession.setAttribute("administrators", administrators);
			return "/jsp/loginIng";
		}else{
			return "/jsp/loginError";
		}
		
		
	}
}
