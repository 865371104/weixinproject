package cn.toe.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.toe.bean.Galaxy;
import cn.toe.bean.Planet;
import cn.toe.service.GalaxyService;
import cn.toe.service.PlanetService;

@Controller
@RequestMapping("galaxy")
public class GalaxyController {
	
	@Autowired
	private GalaxyService galaxyService;
	
	@Autowired
	private PlanetService planetService;
	
	@RequestMapping("/toAddPlanet")
	public String toAddPlanet(HttpServletRequest request){
		List<Galaxy> galaxys = galaxyService.getAllGalaxy();
		request.setAttribute("galaxys", galaxys);
		return "jsp/Galaxy/addPlanet";
	}
	
	@RequestMapping("/addGalaxy")
	public String addGalaxy(Galaxy galaxy){
		galaxyService.addGalaxy(galaxy);		
		return "jsp/Galaxy/galaxyManager";
	}
	
	@RequestMapping("/addPlanet")
	public String addPlanet(Planet planet,Integer galaxys){
		planet.setGalaxy(galaxyService.getGalaxy(galaxys));
		System.out.println(planet.getGalaxy().getGalaxyName());
		planetService.addPlanet(planet);	
		return "jsp/Galaxy/addPlanet";
	}
	
	@RequestMapping("/getPlanetbyGa")
	public String getgetPlanetbyGa(HttpServletRequest request,Integer galaxy){
		List<Planet> planet = planetService.getPlanetbyGa(galaxy);
		List<Galaxy> galaxys = galaxyService.getAllGalaxy();
		request.setAttribute("galaxys", galaxys);
		request.setAttribute("galaxy", galaxy);
		request.setAttribute("planet", planet);
		return "jsp/Galaxy/planetManager";
	}
	
	@RequestMapping("/getAllGalaxy")
	public String getAllGalaxy(HttpServletRequest request){
		List<Galaxy> galaxy = galaxyService.getAllGalaxy();
		request.setAttribute("galaxy", galaxy);
		return "jsp/Galaxy/galaxyManager";
	}
	
	@RequestMapping("/getAllPlanet")
	public String getgetPlanetbyGa(HttpServletRequest request){
		List<Planet> planet = planetService.getAllPlanet();
		List<Galaxy> galaxys = galaxyService.getAllGalaxy();
		request.setAttribute("planet", planet);
		request.setAttribute("galaxys", galaxys);
		return "jsp/Galaxy/planetManager";
	}
	
	@RequestMapping("/getGalaxytoedit")
	public String getGalaxytoedit(Integer id,HttpServletRequest request){
		Galaxy galaxy = galaxyService.getGalaxy(id);
		request.setAttribute("galaxy", galaxy);
		return "/jsp/Galaxy/editGalaxy";
	}
	
	@RequestMapping("/getPlanettoedit")
	public String getPlanettoedit(Integer id,HttpServletRequest request){
		Planet planet = planetService.getPlanet(id);
		request.setAttribute("planet", planet);
		return "/jsp/Galaxy/editPlanet";
	}
	
	@RequestMapping("/updateGalaxy")
	public String updateUser(Galaxy galaxy,HttpServletRequest request){
		if(galaxyService.updateGalaxy(galaxy)){
			request.setAttribute("galaxy", galaxy);
			return "/editGalaxy";
		}else{
			return "/error";
		}
	}
	
	@RequestMapping("/updatePlanet")
	public String updateUser(Planet planet,HttpServletRequest request){
		if(planetService.updatePlanet(planet)){
			request.setAttribute("planet", planet);
			return "/editplanet";
		}else{
			return "/error";
		}
	}
	

	@RequestMapping("/delGalaxy")
	public void delGalaxy(String id,HttpServletResponse response){
		boolean i = false;
		try {
			i = galaxyService.delGalaxy(Integer.parseInt(id));
		} catch (Exception e) {
			// TODO: handle exception
		}
		String result = "{\"result\":\"error\"}";
		if(i){
			result = "{\"result\":\"success\"}";
		}
		PrintWriter out = null;
		response.setContentType("application/json");
		
		try {
			out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	@RequestMapping("/delPlanet")
	public void delPlanet(String id,HttpServletResponse response){
		boolean i = false;
		try {
			i = planetService.delPlanet(Integer.parseInt(id));
		} catch (Exception e) {
			// TODO: handle exception
		}
		String result = "{\"result\":\"error\"}";
		if(i){
			result = "{\"result\":\"success\"}";
		}
		PrintWriter out = null;
		response.setContentType("application/json");
		
		try {
			out = response.getWriter();
			out.write(result);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
