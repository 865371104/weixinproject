package cn.toe.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.toe.bean.Administrators;
import cn.toe.service.AdministratorsService;

@Controller
@RequestMapping("/AdminSet")
public class AdminSetController {
	
	@Autowired
	private AdministratorsService administratorsService;
	
	@RequestMapping("/toResetPWD")
	private String toResetPWD(HttpServletRequest request){
		HttpSession mySession = request.getSession();
		if(mySession.getAttribute("administrators")!=null){
			return "redirect:/jsp/Adminmsg/resetPWD.jsp";
		}else{
			return "redirect:/jsp/Administrators/login.jsp";
		}	
	}
	
	@RequestMapping("/resetPWD")
	private String resetPWD(String newpwd,HttpServletRequest request){
		HttpSession mySession = request.getSession();
		if(mySession.getAttribute("administrators")!=null){
			Integer adminid = ((Administrators)mySession.getAttribute("administrators")).getAdminid();
			Administrators administrators = administratorsService.getAdministrators(adminid);
			administrators.setPassWord(newpwd);
			administratorsService.updateAdministrators(administrators);
			mySession.setAttribute("resetResultMsg", "修改密码成功");
			mySession.setAttribute("administrators", administrators);
		}else{
			mySession.setAttribute("resetResultMsg", "修改密码失败");
		}
		return "redirect:/jsp/Adminmsg/resetPWD.jsp";
	}
	
	@RequestMapping("/seeAdminMsg")
	private String seeAdminMsg(){
		return "";
	}
	
	@RequestMapping("/updateAdminMsg")
	private String updateAdminMsg(Administrators administrators,HttpServletRequest request){
		HttpSession mySession = request.getSession();
		Integer adminid = ((Administrators)mySession.getAttribute("administrators")).getAdminid();
		Administrators admin = administratorsService.getAdministrators(adminid);
		admin.setDocumentType(administrators.getDocumentType());
		admin.setDocumentId(administrators.getDocumentId());
		admin.setEmail(administrators.getEmail());
		admin.setTphone(administrators.getTphone());
		try {
			if(administratorsService.updateAdministrators(admin)){
				mySession.setAttribute("updateAdminMsg", "修改信息成功");
				mySession.setAttribute("administrators", administrators);
			}else{
				mySession.setAttribute("updateAdminMsg", "修改信息失败");
			}
		} catch (Exception e) {
			// TODO: handle exception
			mySession.setAttribute("updateAdminMsg", "修改信息失败");
		}
		return "redirect:/jsp/Adminmsg/AdminMessage.jsp";
	}
}
