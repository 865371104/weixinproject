package cn.toe.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.toe.bean.Administrators;
import cn.toe.service.AdministratorsService;
import cn.toe.service.RoleService;

@Controller
@RequestMapping("/List")
public class ListController {
	
	@Autowired
	private AdministratorsService administratorsService;
	
	@Autowired
	private RoleService roleService;
	
	@RequestMapping("/getList")
	public String getList(HttpServletRequest request){
		HttpSession mySession = request.getSession();
		Administrators administrators = (Administrators)mySession.getAttribute("administrators");
		return "/jsp/WestList/List";
	}
}
