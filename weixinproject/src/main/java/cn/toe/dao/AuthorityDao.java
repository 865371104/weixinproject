package cn.toe.dao;

import java.util.List;

import cn.toe.bean.Authority;

public interface AuthorityDao {

	public void addAuthority(Authority authority);  
	
	public List<Authority> getAllAuthority();
	
	public List<Authority> getAllAuthoritybyNull();
	
	public List<Authority> getAllAuthoritybyNoNull();
	
	public boolean delAuthority(Integer id);
	
	public Authority getAuthority(Integer id);
	
	public boolean updateAuthority(Authority authority);
}
