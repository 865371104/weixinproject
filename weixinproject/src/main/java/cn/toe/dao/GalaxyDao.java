package cn.toe.dao;

import java.util.List;

import cn.toe.bean.Galaxy;

public interface GalaxyDao {
	
	public void addGalaxy(Galaxy galaxy);  
	
	public List<Galaxy> getAllGalaxy();
	
	public boolean delGalaxy(Integer id);
	
	public Galaxy getGalaxy(Integer id);
	
	public boolean updateGalaxy(Galaxy galaxy);
}
