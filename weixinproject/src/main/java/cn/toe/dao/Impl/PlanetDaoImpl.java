package cn.toe.dao.Impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.toe.bean.Galaxy;
import cn.toe.bean.Planet;
import cn.toe.dao.PlanetDao;

@Repository
public class PlanetDaoImpl implements PlanetDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addPlanet(Planet planet) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(planet);
	}

	@Override
	public List<Planet> getAllPlanet() {
		// TODO Auto-generated method stub
		String hql = "from Planet";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}
	
	@Override
	public List<Planet> getPlanetbyGa(Integer galaxy) {
		// TODO Auto-generated method stub
		String hql = "from Planet p where p.galaxy=:galaxy";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setInteger("galaxy",galaxy);
		System.out.println(query.list().size());
		return query.list();
	}

	@Override
	public boolean delPlanet(Integer id) {
		String hql = "delete Planet p where p.planetid=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setInteger(0, id);
		return (query.executeUpdate() > 0);
	}

	@Override
	public Planet getPlanet(Integer id) {
		// TODO Auto-generated method stub
		String hql = "from Planet p where p.planetid=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);//此时使用的是hql语句
		query.setInteger(0, id);
		return (Planet) query.uniqueResult();
	}

	@Override
	public boolean updatePlanet(Planet planet) {
		// TODO Auto-generated method stub
		String hql = "update Planet p set p.planename=:planename,p.ruler=:ruler,p.radius=:radius,p.quality=:quality,p.cycle=:cycle,p.sma=:sma,p.cro=:cro,p.dipangle=:dipangle,p.dm=:dm,p.date=:date,p.distance=:distance,p.galaxy=:galaxy,p.numvar=:numvar,p.img=:img,p.note=:note where p.planetid=:planetid";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setString("planename", planet.getPlanename());
		query.setInteger("ruler", planet.getRuler());
		query.setDouble("radius", planet.getRadius());
		query.setDouble("quality", planet.getQuality());
		query.setDouble("cycle", planet.getCycle());
		query.setDouble("sma", planet.getSma());
		query.setDouble("cro", planet.getCro());
		query.setDouble("dipangle", planet.getDipangle());
		query.setString("dm", planet.getDm());
		query.setDate("date", planet.getDate());
		query.setDouble("distance", planet.getDistance());
		query.setInteger("galaxy",planet.getGalaxy().getGalaxyid());
		query.setString("numvar", planet.getNumvar());
		query.setString("img", planet.getImg());
		query.setString("note", planet.getNote());
		query.setInteger("planetid", planet.getPlanetid());
		return (query.executeUpdate() > 0);
	}

	

	

}
