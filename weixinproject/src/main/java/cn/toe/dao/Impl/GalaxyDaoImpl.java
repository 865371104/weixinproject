package cn.toe.dao.Impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.toe.bean.Galaxy;
import cn.toe.dao.GalaxyDao;

@Repository
public class GalaxyDaoImpl implements GalaxyDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addGalaxy(Galaxy galaxy) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(galaxy);
	}

	@Override
	public List<Galaxy> getAllGalaxy() {
		// TODO Auto-generated method stub
		String hql = "from Galaxy";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

	@Override
	public boolean delGalaxy(Integer id) {
		// TODO Auto-generated method stub
		String hql = "delete Galaxy g where g.galaxyid=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setInteger(0, id);
		return (query.executeUpdate() > 0);
	}

	@Override
	public Galaxy getGalaxy(Integer id) {
		// TODO Auto-generated method stub
		String hql = "from Galaxy g where g.galaxyid=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);//此时使用的是hql语句
		query.setInteger(0, id);
		return (Galaxy) query.uniqueResult();
	}

	@Override
	public boolean updateGalaxy(Galaxy galaxy) {
		// TODO Auto-generated method stub
		String hql = "update Galaxy g set g.galaxyName=:galaxyName,g.galaxyIN=:galaxyIN,g.galaxyNum=:galaxyNum,g.type=:type,g.img=:img,g.fyear=:fyear,g.note=:note,g.ruler=:ruler where g.galaxyid=:galaxyid";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setString("galaxyName",galaxy.getGalaxyName());
		query.setString("galaxyIN",galaxy.getGalaxyIN());
		query.setString("galaxyNum",galaxy.getGalaxyNum());
		query.setString("type",galaxy.getType());
		query.setString("img",galaxy.getImg());
		query.setDate("fyear",galaxy.getFyear());
		query.setString("note",galaxy.getNote());
		query.setInteger("ruler",galaxy.getRuler().getPlanetid());
		query.setInteger("galaxyid",galaxy.getGalaxyid());
		return (query.executeUpdate() > 0);
	}

	

}
