package cn.toe.dao.Impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.toe.bean.Role;
import cn.toe.dao.RoleDao;

@Repository
public class RoleDaoImpl implements RoleDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addRole(Role role) {
		sessionFactory.getCurrentSession().save(role);
		System.err.println(role.getContent());
		String hql = "from Role r where r.role=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);//此时使用的是hql语句
		query.setInteger(0, role.getRole());
		Role role1 = (Role) query.uniqueResult();
		System.err.println(role1.getContent());
	}

	@Override
	public List<Role> getAllRole() {
		// TODO Auto-generated method stub
		String hql = "from Role";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

	@Override
	public boolean delRole(Integer id) {
		// TODO Auto-generated method stub
		String hql = "delete Role r where r.role=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setInteger(0, id);
		return (query.executeUpdate() > 0);
	}

	@Override
	public Role getRole(Integer id) {
		String hql = "from Role r where r.role=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);//此时使用的是hql语句
		query.setInteger(0, id);
		return (Role) query.uniqueResult();
	}
	
	public Role getRoleByname(String name){
		String hql = "from Role r where r.content=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);//此时使用的是hql语句
		query.setString(0, name);
		return (Role) query.uniqueResult();
	}

	@Override
	public boolean updateRoleofhql(Role role) {
		// TODO Auto-generated method stub
		String hql = "update Role r set r.content=:content where r.role=:role";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setString("content", role.getContent());
		query.setInteger("role", role.getRole());
		return (query.executeUpdate() > 0);
	}

}
