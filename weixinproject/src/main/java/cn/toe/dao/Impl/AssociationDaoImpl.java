package cn.toe.dao.Impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.toe.dao.AssociationDao;
import cn.toe.bean.Association;
import cn.toe.bean.Astyle;

@Repository
public class AssociationDaoImpl implements AssociationDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addAssociation(Association association) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(association);
	}

	@Override
	public List<Association> getAllAssociation() {
		// TODO Auto-generated method stub
		String hql = "from Association";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

	@Override
	public boolean delAssociation(Integer id) {
		// TODO Auto-generated method stub
		String hql = "delete Association a where a.association_id=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setInteger(0, id);
		return (query.executeUpdate() > 0);
	}

	@Override
	public Association getAssociationofhql(Integer id) {
		// TODO Auto-generated method stub
		String hql = "from Association a where a.association_id=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);//此时使用的是hql语句
		query.setInteger(0, id);
		return (Association) query.uniqueResult();
	}

	@Override
	public boolean updateAssociation(Association association) {
		// TODO Auto-generated method stub
		String hql = "update Association a set a.aname=:aname,a.url=:url,a.type=:type,a.address=:address,a.phone=:phone,a.note=:note,a.cdate=:cdate,a.ceo=:ceo where a.associationId=:associationId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setString("aname",association.getAname());
		query.setString("url",association.getUrl());
		query.setString("type",association.getType());
		query.setString("address",association.getAddress());
		query.setString("phone",association.getPhone());
		query.setString("note",association.getNote());
		query.setDate("cdate",association.getCdate());
		query.setString("ceo",association.getCeo());
		query.setInteger("associationId",association.getAssociationId());
		return (query.executeUpdate() > 0);
	}

	

}
