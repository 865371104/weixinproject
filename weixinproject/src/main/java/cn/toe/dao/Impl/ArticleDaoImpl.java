package cn.toe.dao.Impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.toe.bean.Article;
import cn.toe.bean.Association;
import cn.toe.dao.ArticleDao;

@Repository
public class ArticleDaoImpl implements ArticleDao {

	@Autowired
	private SessionFactory sessionFactory;	
	
	@Override
	public void addArticle(Article article) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(article);
	}

	@Override
	public List<Article> getAllArticle() {
		// TODO Auto-generated method stub
		String hql = "from Article";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

	@Override
	public boolean delArticle(Integer id) {
		// TODO Auto-generated method stub
		String hql = "delete Article a where a.articleId=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setInteger(0, id);
		return (query.executeUpdate() > 0);
	}

	@Override
	public Article getArticle(Integer id) {
		// TODO Auto-generated method stub
		String hql = "from Article a where a.articleId=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);//此时使用的是hql语句
		query.setInteger(0, id);
		return (Article) query.uniqueResult();
	}

	@Override
	public boolean updateArticle(Article article) {
		// TODO Auto-generated method stub
		String hql = "update Article a set a.title=:title,a.articleContent=:articleContent,a.institutions=:institutions,a.editor=:editor,a.img=:img,a.note=:note where a.articleId=:articleId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setString("title",article.getTitle());
		query.setString("articleContent",article.getArticleContent());
		query.setString("institutions",article.getInstitutions());
		query.setString("editor",article.getEditor());
		query.setString("img",article.getImg());
		query.setString("note",article.getNote());
		//query.setInteger("astyle",article.getAstyle().getAstyleId());
		query.setInteger("articleId",article.getArticleId());
		return (query.executeUpdate() > 0);
	}

	
}
