package cn.toe.dao.Impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.toe.bean.Astyle;
import cn.toe.bean.Galaxy;
import cn.toe.dao.AstyleDao;


@Repository
public class AstyleDaoImpl implements AstyleDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addAstyle(Astyle astyle) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(astyle);
	}

	@Override
	public List<Astyle> getAllAstyle() {
		// TODO Auto-generated method stub
		String hql = "from Astyle";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

	@Override
	public boolean delAstyle(Integer id) {
		// TODO Auto-generated method stub
		String hql = "delete Astyle a where a.astyleId=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setInteger(0, id);
		return (query.executeUpdate() > 0);
	}

	@Override
	public Astyle getAstyle(Integer id) {
		// TODO Auto-generated method stub
		String hql = "from Astyle a where a.astyleId=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);//此时使用的是hql语句
		query.setInteger(0, id);
		return (Astyle) query.uniqueResult();
	}
	
	@Override
	public Astyle getAstyleByName(String name) {
		// TODO Auto-generated method stub
		String hql = "from Astyle a where a.sname=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);//此时使用的是hql语句
		query.setString(0, name);
		return (Astyle) query.uniqueResult();
	}

	@Override
	public boolean updateAstyle(Astyle astyle) {
		// TODO Auto-generated method stub
		String hql = "update Astyle a set a.sname=:sname where a.astyleId=:astyleId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setString("sname",astyle.getSname());
		query.setInteger("astyleId", astyle.getAstyleId());
		return (query.executeUpdate() > 0);
	}

	

}
