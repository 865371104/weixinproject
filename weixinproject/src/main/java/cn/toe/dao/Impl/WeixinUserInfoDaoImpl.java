package cn.toe.dao.Impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.toe.bean.WeixinUserInfo;
import cn.toe.dao.WeixinUserInfoDao;

@Repository
public class WeixinUserInfoDaoImpl implements WeixinUserInfoDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addWeixinUserInfo(WeixinUserInfo weixinUserInfo) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(weixinUserInfo);
	}

	@Override
	public List<WeixinUserInfo> getAllWeixinUserInfo() {
		// TODO Auto-generated method stub
		String hql = "from WeixinUserInfo";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

	@Override
	public boolean delWeixinUserInfo(Integer id) {
		// TODO Auto-generated method stub
		String hql = "delete WeixinUserInfo u where u.userId=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setInteger(0, id);
		return (query.executeUpdate() > 0);
	}

	@Override
	public WeixinUserInfo getWeixinUserInfo(Integer id) {
		// TODO Auto-generated method stub
		String hql = "from WeixinUserInfo u where u.userId=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);//此时使用的是hql语句
		query.setInteger(0, id);
		return (WeixinUserInfo)query.uniqueResult();
	}
	
	@Override
	public WeixinUserInfo getWeixinUserInfoByOpenId(String openid) {
		// TODO Auto-generated method stub
		String hql = "from WeixinUserInfo u where u.openId=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);//此时使用的是hql语句
		query.setString(0, openid);
		return (WeixinUserInfo)query.uniqueResult();
	}

	@Override
	public boolean updateWeixinUserInfo(WeixinUserInfo weixinUserInfo) {
		// TODO Auto-generated method stub
		String hql = "update WeixinUserInfo w set w.openId=:openId,w.subscribe=:subscribe,w.subscribeTime=:subscribeTime,w.nickName=:nickName,w.sex=:sex,w.country=:country,w.province=:province,w.city=:city,w.language=:language,w.headImgUrl=:headImgUrl,w.adminid=:adminid where w.userId=:userId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setString("openId",weixinUserInfo.getOpenId());
		query.setInteger("subscribe",weixinUserInfo.getSubscribe());
		query.setString("subscribeTime",weixinUserInfo.getSubscribeTime());
		query.setString("nickName",weixinUserInfo.getNickName());
		query.setInteger("sex",weixinUserInfo.getSex());
		query.setString("country",weixinUserInfo.getCountry());
		query.setString("province",weixinUserInfo.getProvince());
		query.setString("city",weixinUserInfo.getCity());
		query.setString("language",weixinUserInfo.getLanguage());
		query.setString("headImgUrl",weixinUserInfo.getHeadImgUrl());
		if(weixinUserInfo.getAdminid()==null){
			query.setBigInteger("adminid",null);
		}else{
			query.setInteger("adminid",weixinUserInfo.getAdminid().getAdminid());
		}
		query.setInteger("userId",weixinUserInfo.getUserId());
		return (query.executeUpdate() > 0);
	}

}
