package cn.toe.dao.Impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.toe.bean.Authority;
import cn.toe.dao.AuthorityDao;

@Repository
public class AuthorityDaoImpl implements AuthorityDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void addAuthority(Authority authority) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(authority);
	}

	@Override
	public List<Authority> getAllAuthority() {
		// TODO Auto-generated method stub
		String hql = "from Authority";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

	@Override
	public List<Authority> getAllAuthoritybyNull(){
		String hql = "from Authority a where a.authorityparent=null";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
		
	}
	
	@Override
	public List<Authority> getAllAuthoritybyNoNull(){
		String hql = "from Authority a where a.authorityparent!=null";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
		
	}
	
	@Override
	public boolean delAuthority(Integer id) {
		// TODO Auto-generated method stub
		String hql = "delete Authority a where a.authorityId=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setInteger(0, id);
		return (query.executeUpdate() > 0);
	}

	@Override
	public Authority getAuthority(Integer id) {
		// TODO Auto-generated method stub
		String hql = "from Authority a where a.authorityId=?";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);//此时使用的是hql语句
		query.setInteger(0, id);
		return (Authority) query.uniqueResult();
	}

	@Override
	public boolean updateAuthority(Authority authority) {
		// TODO Auto-generated method stub
		String hql = "update Authority a set a.authorityName=:authorityName,a.authorityContent=:authorityContent where a.authorityId=:authorityId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setString("authorityName",authority.getAuthorityName());
		query.setString("authorityContent",authority.getAuthorityContent());
		query.setInteger("authorityId",authority.getAuthorityId());
		return query.executeUpdate() > 0;
	}

}
