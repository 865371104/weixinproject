package cn.toe.dao;

import java.util.List;

import cn.toe.bean.WeixinUserInfo;;

public interface WeixinUserInfoDao {
	//保存用户
	public void addWeixinUserInfo(WeixinUserInfo weixinUserInfo);  
	
	//获取整个用户列表
	public List<WeixinUserInfo> getAllWeixinUserInfo();
	
	//删除用户
	public boolean delWeixinUserInfo(Integer id);
	
	//根据ID获得用户
	public WeixinUserInfo getWeixinUserInfo(Integer id);
	
	//根据opinid获得用户
	public WeixinUserInfo getWeixinUserInfoByOpenId(String openid);
	
	//更新用户
	public boolean updateWeixinUserInfo(WeixinUserInfo weixinUserInfo);
}

