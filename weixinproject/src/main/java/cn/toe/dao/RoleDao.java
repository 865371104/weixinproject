package cn.toe.dao;

import java.util.List;

import cn.toe.bean.Role;

public interface RoleDao {

	public void addRole(Role role);  
	
	public List<Role> getAllRole();
	
	public boolean delRole(Integer id);
	
	public Role getRole(Integer id);
	
	public Role getRoleByname(String name);
	
	public boolean updateRoleofhql(Role role);
}
