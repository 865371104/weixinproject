package cn.toe.dao;

import java.util.List;

import cn.toe.bean.Association;

public interface AssociationDao {

	public void addAssociation(Association association);  
	
	public List<Association> getAllAssociation();
	
	public boolean delAssociation(Integer id);
	
	public Association getAssociationofhql(Integer id);
	
	public boolean updateAssociation(Association association);
}
