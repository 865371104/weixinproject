package cn.toe.dao;

import java.util.List;

import cn.toe.bean.Galaxy;
import cn.toe.bean.Planet;


public interface PlanetDao {
	
	public void addPlanet(Planet planet);  
	
	public List<Planet> getAllPlanet();
	
	public List<Planet> getPlanetbyGa(Integer id);
	
	public boolean delPlanet(Integer id);
	
	public Planet getPlanet(Integer id);
	
	public boolean updatePlanet(Planet planet);
}
