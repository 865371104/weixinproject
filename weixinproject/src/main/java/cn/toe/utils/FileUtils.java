package cn.toe.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
	public static List<String> readFile(String filename) 
	{
		List<String> allLine = new ArrayList<String>();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(new File(filename)));
			String str = null;
			while ((str = reader.readLine()) != null) {
					allLine.add(str);
			}	
		}catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return allLine;
	}
	public static void writeFile(String filename, String str){
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(new File(filename)));
			writer.write(str);
			writer.flush();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}

}
