<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>registered</title>
</head>
	<link rel="stylesheet" type="text/css" href="css/registered.css">
	<link rel="stylesheet" type="text/css" href="jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="jquery-easyi-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="jquery-easyui-1.5/jquery.easyui.min.js"></script>
<body id="gradient">
	<div id="w" class="easyui-window" title="registered" data-options="iconCls:'icon-save',collapsible:false,minimizable:false,maximizable:false,closable:false" style="width:500px;height:300px;">
		<form id="ff" method="post">
			<div style="margin-bottom:20px;margin-top: 20px;margin-left: 70px">
				<input class="easyui-textbox" prompt="请输入昵称" iconWidth="28" style="width:80%;height:34px;padding:10px;">
			</div>
			<div style="margin-bottom:20px;margin-left: 70px">
				<input id="vv" class="easyui-textbox" prompt="请输入联系邮箱" style="width:80%;height:34px;padding:10px;" data-options="required:true,validType:'email'">
				
			</div>
			<div style="margin-bottom:20px;margin-left: 70px">
				<input id="dd" type="text" class="easyui-datebox" prompt="请选择生日日期" required="required">
				<script type="text/javascript">
			    	$.extend($.fn.datebox.defaults.formatter = function(date){
			    		var y = date.getFullYear();
			    		var m = date.getMonth()+1;
			    		var d = date.getDate();
			    		return y+'-'+m+'-'+d;
			  		});
			    	
			        $.fn.datebox.defaults.parser = function(s){
			        	var t = Date.parse(s);
			        	if (!isNaN(t)){
			        		return new Date(t);
			        	} else {
			        		return new Date();
			        	}
			        };
		    	</script>
			</div>
			<div style="margin-bottom:20px;margin-left: 70px">
				<input id="sex" type="text" class="easyui-combotree" prompt="请选择性别" url="../js/sex.json" required="required">
			</div>
			<div style="text-align:center;padding:5px 0">
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">提交</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">清除</a>
				<p>
				<a href="../login.jsp">返回登陆页面</a>
			</div>  	
		</form>
			
	</div>
	<script>
		function submitForm(){
			$('#ff').form('submit');
		}
		function clearForm(){
			$('#ff').form('clear');
		}
		
	</script>
	</div>
</body>
</html>