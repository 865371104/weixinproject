<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>login</title>
	<link rel="stylesheet" type="text/css" href="css/login.css">
	<link rel="stylesheet" type="text/css" href="jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="jquery-easyi-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="jquery-easyui-1.5/jquery.easyui.min.js"></script>
</head>
<body id="gradient">
	<!-- <div id="gradient" style="width:100%;height: 100%"> -->
		<div id="w" class="easyui-window" title="Login" data-options="iconCls:'icon-save',collapsible:false,minimizable:false,maximizable:false,closable:false" style="width:500px;height:200px;">
			<form id="ff" method="post">
				<div style="margin-bottom:20px;margin-top: 20px;margin-left: 70px">
					<input class="easyui-textbox" prompt="Username" iconWidth="28" style="width:80%;height:34px;padding:10px;">
				</div>
				<div style="margin-bottom:20px;margin-left: 70px">
					<input class="easyui-passwordbox" prompt="Password" iconWidth="28" style="width:80%;height:34px;padding:10px">
				</div>
				<div style="text-align:center;padding:5px 0">
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">Submit</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">Clear</a>
					<p>
					<a>registered</a>
					<a>forget password</a>
				</div>  	
			</form>
			
		</div>
	<!-- </div> -->
	<script>
		function submitForm(){
			$('#ff').form('submit');
		}
		function clearForm(){
			$('#ff').form('clear');
		}
	</script>

	
</body>
</html>