<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
	if (session.getAttribute("updateroleContent") != null) {
%>
<script type="text/javascript">
	alert("${updateroleContent }");
</script>
<%
	session.setAttribute("updateroleContent", null);
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>权限列表</title>
<link rel="stylesheet" type="text/css" href="../css/login.css">
<link rel="stylesheet" type="text/css"
	href="../jquery-easyui-1.5/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="../jquery-easyi-1.5/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="../jquery-easyui-1.5/demo/demo.css">
<script type="text/javascript" src="../jquery-easyui-1.5/jquery.min.js"></script>
<script type="text/javascript"
	src="../jquery-easyui-1.5/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="../jquery-easyui-1.5/customVerification.js"></script>
<script type="text/javascript"
	src="../jquery-easyui-1.5/locale/easyui-lang-zh_CN.js"></script>

<script type="text/javascript">
	$.extend($.fn.validatebox.defaults.rules, {
		checkOpenIda : {
			validator : function(value, param) {
				var haha = " ";
				$.ajax({
					type : 'post',
					async : false,
					url : 'http://localhost:8080/weixin/Admin/checkOpenIda.do',
					data : {
						"name" : value
					},
					success : function(data) {
						haha = data;
					}
				});
				return haha.indexOf("true");
			},
			message : '没有该Openid的微信用户'
		},

		checkOpenIdb : {
			validator : function(value, param) {
				var haha = " ";
				$.ajax({
					type : 'post',
					async : false,
					url : 'http://localhost:8080/weixin/Admin/checkOpenIdb.do',
					data : {
						"name" : value
					},
					success : function(data) {
						haha = data;
					}
				});
				return haha.indexOf("true");
			},
			message : '该Openid的微信用户已经是管理员了'
		},
		
		checkAdminname : {
			validator : function(value, param) {
				var haha = " ";
				$.ajax({
					type : 'post',
					async : false,
					url : 'http://localhost:8080/weixin/Admin/checkAdminname.do',
					data : {
						"name" : value
					},
					success : function(data) {
						haha = data;
					}
				});
				return haha.indexOf("true");
			},
			message : '此账号已经被占用'
		}
	});

	function check() {
		var name1 = $("#openid").val().trim();
		var name2 = $("#adminname").val().trim();
		$.ajax({
			type : 'post',
			async : false,
			url : 'http://localhost:8080/weixin/Admin/check.do',
			data : {
				"name1" : name1,
				"name2" : name2
			},
			success : function(data) {
			}
		});
		submitForm();
		outcheck();
	}
	function outcheck() {
		$.ajax({
			type : 'post',
			async : false,
			url : 'http://localhost:8080/weixin/Admin/outcheck.do',
			data : {},
			success : function(data) {
			}
		});
	}
</script>
</head>
<body id="gradient">
	<form id="ff1" method="post">
		<table cellpadding="5">
			<tr>
				<td>微信OpenID</td>
				<td><input id="openid" class="easyui-textbox" type="text" name="openid" style="width:200px;height:34px;padding:10px;"
					data-options="validType:['checkOpenIda','checkOpenIdb']"></input></td>
			</tr>
			<tr>
				<td>管理账号:</td>
				<td><input id="adminname" class="easyui-textbox" type="text" name="adminname" style="width:200px;height:34px;padding:10px;"
					data-options="required:true,validType:'checkAdminname'"></input></td>
			</tr>
			<tr>
				<td>密码：</td>
				<td><input id="pwd" class="easyui-textbox" type="text" name="pwd" style="width:200px;height:34px;padding:10px;"
					data-options="required:true"></input></td>
			</tr>
			<tr>
				<td rowspan="2">
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="check()" style="width:70px">提交</a>
				</td>
			</tr>
		</table>
	</form>

	<script type="text/javascript">
		function submitForm() {
			$('#ff1').form('submit', {
				onSubmit : function() {
					return $(this).form('enableValidation').form('validate');
				},
				success : function(data) {
					if (data.success == 'ture') {
						$.messager.alert('提示', data.msg, 'info');
					} else {
						//验证通过后跳转页面
						var form = document.forms[0];
						form.action = "/weixin/Admin/addAdmin.do";
						form.method = "post";
						form.submit();
						parent.$('#dg').datagrid('reload');
						parent.$('#addAdmin-win').window('close');
					}
				}
			});
		}
	</script>



</body>
</html>