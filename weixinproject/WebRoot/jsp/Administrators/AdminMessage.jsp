<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>管理员列表</title>
	<link rel="stylesheet" type="text/css" href="../css/login.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/customVerification.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/locale/easyui-lang-zh_CN.js"></script>
	
	<script type="text/javascript">
	function delAdmin(id){
		$.ajax({
        	type : 'post',
        	async : false,
        	url : 'http://localhost:8080/weixin/Admin/delAdmin.do',
        	data : {
            	"id" : id
        	},
        	success : function(data) {
        		if("success" == data.result){
    				alert("删除成功!");
    				$('#dg').datagrid('reload');
    			}else{
    				alert("删除失败!")
    			}
        	}
    	});
	}
	
	</script>

<%	
	if(session.getAttribute("administrators")!=null){
%>
	<script type="text/javascript">
	</script>
<%			
	}
%>	

<%	
	if(session.getAttribute("updateAdminMsg")!=null){
%>
	<script type="text/javascript">
		alert("${updateAdminMsg }");
	</script>
<%			
		session.setAttribute("updateAdminMsg", null);
	}
%>		
</head>
<body id="gradient">
	<table id="dg" title="管理员列表" style="width:800px;height:500px"
			data-options="rownumbers:true,singleSelect:true,pagination:true,url:'toAllAdminMsgList.do',method:'get'">
		<thead>
			<tr>
				<th data-options="field:'adname',width:150">管理员账户</th>
				<th data-options="field:'documentType',width:150">证件类型</th>
				<th data-options="field:'documentId',width:150">证件号码</th>
				<th data-options="field:'email',width:150">电子邮箱</th>
				<th data-options="field:'tphone',width:150">联系电话</th>
			</tr>
		</thead>
	</table>
	<script type="text/javascript">
		$(function(){
			var pager = $('#dg').datagrid().datagrid('getPager');	// get the pager of datagrid
			pager.pagination({
				buttons:[{
					iconCls:'icon-search',
					handler:function(){
						alert('search');
					}
				},{
					iconCls:'icon-add',
					handler:function(){
						$("#addAdmin-win").window('open');
						//alert('add');
					}
				},{
					iconCls:'icon-remove',
					handler:function(){
						var row = $('#dg').datagrid('getSelected');
						if (row){
							delAdmin(row.adminid);
						}
					}
				},{
					iconCls:'icon-edit',
					handler:function(){
						var row = $('#dg').datagrid('getSelected');
						if (row){
							alert('管理员账户:'+row.adname+"数据库id:"+row.adminid);
						}
						//alert('edit');
					}
				}]
			});			
		})
	</script>
	
	<div id="addAdmin-win" class="easyui-window" resizable="false" title="新建角色" closed="true" style="width:500px;height:300px;overflow:hidden;">	
		<iframe scrolling="no" noresize="noresize" frameborder="0" src="/weixin/Admin/toAddAdmin.do" name="addrole-conter" width="100%" height="100%" seamless></iframe>
	</div>
	
	
	
</body>
</html>