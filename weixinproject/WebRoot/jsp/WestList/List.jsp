<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin Window</title>
	<link rel="stylesheet" type="text/css" href="../css/index.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyi-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.easyui.min.js"></script>
</head>

<body class="easyui-layout">  

	<!-- 左面部分 -->
		<div id="aa" class="easyui-accordion menubutton" style="height:100%;">
			<div title="个人" data-options="iconCls:'icon-reload',selected:true" style="padding:10px;">
    			<ul id="tt" class="easyui-tree">
    				<li>
    					<a href="/weixin/jsp/Adminmsg/AdminMessage.jsp"  target="conter">个人信息</a>
    				</li>
    				<li>
    					<a href="/weixin/AdminSet/toResetPWD.do" target="conter">修改密码</a>
    				</li>
    			</ul>
       		</div>
       		<div title="管理员" data-options="iconCls:'icon-reload',selected:true" style="padding:10px;">
    			<ul id="tt" class="easyui-tree">
    				<li>
    					<a href="/weixin/Admin/toAllAdminMsg.do" target="conter">管理员列表</a>
    				</li>
    				<li>
    					<a href="/weixin/role/getAllRole.do" target="conter">权限管理</a>
    				</li>
    			</ul>
       		</div>
       		<div title="用户" data-options="iconCls:'icon-save'" style="padding:10px;">
    			<ul id="tt" class="easyui-tree">
    				<li>
    					<a href="" target="conter">用户列表</a>
    				</li>
    				<li>
    					<a href="" target="conter">用户列表</a>
    				</li>
    			</ul>
        	</div>
        	<div title="星星" data-options="iconCls:'icon-save'" style="padding:10px;">
    			<ul id="tt" class="easyui-tree">
    				<li>
    					<a href="/weixin/galaxy/getAllGalaxy.do"  target="conter">行星管理</a>
    				</li>
    			</ul>
        	</div>
        	<div title="文章" data-options="iconCls:'icon-save'" style="padding:10px;">
    			<ul id="tt" class="easyui-tree">
    				<li>
    					<a href="/weixin/article/toAddArticle.do"  target="conter">发布文章</a>
    				</li>
    				<li>
    					<a href="/weixin/article/getAllArticle.do"  target="conter">文章列表</a>
    				</li>
    				<li>
    					<a href="/weixin/astyle/toAllAstyle.do"  target="conter">文章类型</a>
    				</li>
    			</ul>
        	</div>
        	<div title="信息" data-options="iconCls:'icon-save'" style="padding:10px;">
    			发布
        	</div>
    	</div>
	
	
</body>
</html>