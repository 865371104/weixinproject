<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>login</title>
	<link rel="stylesheet" type="text/css" href="../css/login.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyi-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/customVerification.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
	$.extend($.fn.validatebox.defaults.rules, {
	    myvalidate : {
	        validator : function(value, param) {
	            var username = $("#username").val().trim();
	            var haha = " ";
	            $.ajax({
	                type : 'post',
	                async : false,
	                url : 'http://toe.haihuogo.com/weixinproject/loginer/checkUN.do',
	                data : {
	                    "username" : username,
	                },
	                success : function(data) {
	                    haha = data;
	                }
	            });
	            return haha.indexOf("true");
	        },
	        message : '用户名不存在'
	    },
	
		checkpwd : {
        	validator : function(value, param) {
          		var username = $("#username").val().trim();
          		var pwd = $("#pwd").val().trim();
            	var haha = " ";
            	$.ajax({
                	type : 'post',
                	async : false,
                	url : 'http://toe.haihuogo.com/weixinproject/loginer/checkPWD.do',
                	data : {
                    	"username" : username,
                    	"pwd" : pwd
                	},
                	success : function(data) {
                    	haha = data;
                	}
            	});
            	return haha.indexOf("true");
        	},
        	message : '密码错误'
    	}
	});
	
	function check(){
		var username = $("#username").val().trim();
  		var pwd = $("#pwd").val().trim();
		$.ajax({
        	type : 'post',
        	async : false,
        	url : 'http://toe.haihuogo.com/weixinproject/loginer/check.do',
        	data : {
            	"username" : username,
            	"pwd" : pwd
        	},
        	success : function(data) {
        	}
    	});
		submitForm();
    	outcheck();
	}
	function outcheck(){
		$.ajax({
        	type : 'post',
        	async : false,
        	url : 'http://toe.haihuogo.com/weixinproject/loginer/outcheck.do',
        	data : {
        	},
        	success : function(data) {
        	}
    	});
	}
	</script>
</head>
<body id="gradient">
	<!-- <div id="gradient" style="width:100%;height: 100%"> -->
		<div id="w" class="easyui-window" title="登入" data-options="iconCls:'icon-save',collapsible:false,minimizable:false,maximizable:false,closable:false" style="width:400px;height:200px;">
			<form id="ff" method="post">
				<div style="margin-bottom:20px;margin-top: 20px;margin-left: 70px">
					<input  id="username" class="easyui-textbox" name="username" prompt="管理员账号" iconWidth="28" style="width:80%;height:34px;padding:10px;"  data-options="required:true,validType:['username','myvalidate']">
				</div>
				<div style="margin-bottom:20px;margin-left: 70px">
					<input id="pwd" class="easyui-passwordbox" prompt="管理员密码" name="pwd" iconWidth="28" style="width:80%;height:34px;padding:10px"  data-options="required:true,validType:'checkpwd'">
				</div>
				<div style="text-align:center;padding:5px 0">
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="check()" style="width:80px">登入</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">清除</a>
				</div>  	
			</form>
			
		</div>
	<!-- </div> -->
	<script>
		function submitForm(){
			$('#ff').form('submit', {
		        onSubmit : function() {
		        	return $(this).form('enableValidation').form('validate');
		        },
		        success:function(data){      
		        if (data.success == 'ture') {   
		             $.messager.alert('提示',data.msg,'info');   
		        }else{   
		            //验证通过后跳转页面
		        	var form = document.forms[0];
		    		form.action="/weixinproject/loginer/login.do";
		    		form.method="post";
		    		form.submit();
		        }     
		      }  
		    });

		}
		function clearForm(){
			$('#ff').form('clear');
		}
	</script>

	
</body>
</html>