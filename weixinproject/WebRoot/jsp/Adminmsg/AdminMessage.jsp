<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>个人信息</title>
	<link rel="stylesheet" type="text/css" href="../../css/login.css">
	<link rel="stylesheet" type="text/css" href="../../jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../../jquery-easyi-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../../jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="../../jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="../../jquery-easyui-1.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../../jquery-easyui-1.5/customVerification.js"></script>
	<script type="text/javascript" src="../../jquery-easyui-1.5/locale/easyui-lang-zh_CN.js"></script>
<%	
	if(session.getAttribute("administrators")!=null){
%>
	<script type="text/javascript">
	</script>
<%			
	}
%>	

<%	
	if(session.getAttribute("updateAdminMsg")!=null){
%>
	<script type="text/javascript">
		alert("${updateAdminMsg }");
	</script>
<%			
		session.setAttribute("updateAdminMsg", null);
	}
%>		
</head>
<body id="gradient">
	<!-- <div id="gradient" style="width:100%;height: 100%"> -->
		<div id="w" style="width:400px;height:200px;">
			<div style="margin-bottom:20px;margin-top: 20px;margin-left: 40px">
				<h1>个人信息</h1>
			</div>
				<div style="margin-bottom:20px;margin-top: 20px;margin-left: 40px">
					<font>管理账户：</font>
					<font>${administrators.adname }</font>
				</div>
				<div style="margin-bottom:20px;margin-top: 20px;margin-left: 40px">
					<font>证件类型：</font>
					<font>${administrators.documentType }</font>
				</div>
				<div style="margin-bottom:20px;margin-top: 20px;margin-left: 40px">
					<font>证件号码：</font>
					<font>${administrators.documentId }</font>
				</div>
				<div style="margin-bottom:20px;margin-top: 20px;margin-left: 40px">
					<font>电子邮箱：</font>
					<font>${administrators.email }</font>
				</div>
				<div style="margin-bottom:20px;margin-top: 20px;margin-left: 40px">
					<font>联系电话：</font>
					<font>${administrators.tphone }</font>
				</div>
				<div style="padding:5px 0;margin-left: 40px" >
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px" >修改信息</a>
				</div> 
			
		</div>
	<!-- </div> -->
	<script>
		function submitForm(){
			window.location.href="/weixin/jsp/Adminmsg/editAdminMsg.jsp"; 
		}

	</script>

	
</body>
</html>