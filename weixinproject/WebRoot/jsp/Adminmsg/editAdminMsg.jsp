<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改密码</title>
	<link rel="stylesheet" type="text/css" href="../../css/login.css">
	<link rel="stylesheet" type="text/css" href="../../jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../../jquery-easyi-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../../jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="../../jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="../../jquery-easyui-1.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../../jquery-easyui-1.5/customVerification.js"></script>
	<script type="text/javascript" src="../../jquery-easyui-1.5/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
	$.extend($.fn.validatebox.defaults.rules, {
		documentType : {
	        validator : function(value, param) {
	            if (value != "") {
                    return value == "身份证"||value == "医疗证"||value == "驾驶证";
                } else {
                    return true;
                }
	        },
	        message : '请输入正确的证件类型(例：身份证)！'
	    },
	
	    documentId : {
        	validator : function(value, param) {
        		var documentType = $("#documentType").val().trim();
        		if (value != "") {
        			return documentType == "身份证"||documentType == "医疗证"||documentType == "驾驶证"
                } else {
                    return true;
                }
        	},
        	message : '请输入正确的证件类型(例：身份证)！'
    	},
    	
    	documentId2 : {
        	validator : function(value, param) {
        		var documentType = $("#documentType").val().trim();
        		if (documentType == "身份证"&&value != "") {
        			return /^\d{15}(\d{2}[A-Za-z0-9])?$/i.test(value);
                } else {
                    return true;
                }
        	},
        	message : '身份证号码格式不正确'
    	}
	});
	
	</script>
</head>
<body id="gradient">
	<!-- <div id="gradient" style="width:100%;height: 100%"> -->
		<div id="w" style="width:400px;height:200px;">
			<div style="margin-bottom:20px;margin-top: 20px;margin-left: 40px">
				<h1>修改信息</h1>
			</div>
			<form id="ff" method="post">
				<input type="hidden" name="adminid" value="${administrators.adminid }">
				<div style="margin-bottom:20px;margin-top: 20px;margin-left: 40px">
					<font>管理账户：</font>
					<input id="adname" class="easyui-textbox" name="adname" iconWidth="28" style="width:80%;height:34px;padding:10px" value="${administrators.adname }" readOnly="true">
				</div>
				<div style="margin-bottom:20px;margin-top: 20px;margin-left: 40px">
					<font>证件类型：</font>
					<input id="documentType" class="easyui-textbox" name="documentType" iconWidth="28" style="width:80%;height:34px;padding:10px" value="${administrators.documentType }" data-options="required:true,validType:'documentType'">
				</div>
				<div style="margin-bottom:20px;margin-top: 20px;margin-left: 40px">
					<font>证件号码：</font>
					<input id="documentId" class="easyui-textbox" name="documentId" iconWidth="28" style="width:80%;height:34px;padding:10px" value="${administrators.documentId }"  data-options="required:true,validType:['documentId','documentId2']">
				</div>
				<div style="margin-bottom:20px;margin-top: 20px;margin-left: 40px">
					<font>电子邮箱：</font>
					<input id="email" class="easyui-textbox" name="email" iconWidth="28" style="width:80%;height:34px;padding:10px" value="${administrators.email }" data-options="required:true,validType:'email'">
				</div>
				<div style="margin-bottom:20px;margin-top: 20px;margin-left: 40px">
					<font>联系电话：</font>
					<input id="tphone" class="easyui-textbox" name="tphone" iconWidth="28" style="width:80%;height:34px;padding:10px" value="${administrators.tphone }" data-options="required:true,validType:'mobile'">
				</div>
				<div style="padding:5px 0;margin-left: 40px" >
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px" >提交修改</a>
				</div>  	
			</form>
			
		</div>
	<!-- </div> -->
	<script>
		function submitForm(){
			$('#ff').form('submit', {
		        onSubmit : function() {
		        	return $(this).form('enableValidation').form('validate');
		        },
		        success:function(data){      
		        if (data.success == 'ture') {   
		             $.messager.alert('提示',data.msg,'info');   
		        }else{   
		            //验证通过后跳转页面
		        	var form = document.forms[0];
		    		form.action="/weixin/AdminSet/updateAdminMsg.do";
		    		form.method="post";
		    		form.submit();
		        }     
		      }  
		    });

		}
	</script>

	
</body>
</html>