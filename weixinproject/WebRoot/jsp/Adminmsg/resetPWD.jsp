<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改密码</title>
	<link rel="stylesheet" type="text/css" href="../../css/login.css">
	<link rel="stylesheet" type="text/css" href="../../jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../../jquery-easyi-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../../jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="../../jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="../../jquery-easyui-1.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../../jquery-easyui-1.5/customVerification.js"></script>
	<script type="text/javascript" src="../../jquery-easyui-1.5/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
	$.extend($.fn.validatebox.defaults.rules, {
		checkoldpwd : {
	        validator : function(value, param) {
	            var pwd = ${administrators.passWord};
	            if (pwd != "" && value != "") {
                    return pwd == value;
                } else {
                    return true;
                }
	        },
	        message : '旧密码错误！'
	    },
	
		checknewpwd : {
        	validator : function(value, param) {
        		var oldpwd = $("#oldpwd").val().trim();
        		if (oldpwd != "" && value != "") {
                    return oldpwd != value;
                } else {
                    return true;
                }
        	},
        	message : '新密码不能与旧密码相同！'
    	},
	    
	    checkconfirmpwd : {
        	validator : function(value, param) {
        		var newpwd = $("#newpwd").val().trim();
        		if (newpwd != "" && value != "") {
                    return newpwd == value;
                } else {
                    return true;
                }
        	},
        	message : '两次输入的密码不一致！'
    	}
	});
	</script>
<%	
	if(session.getAttribute("resetResultMsg")!=null){
%>
	<script type="text/javascript">
		alert("${resetResultMsg }");
	</script>
<%			
		session.setAttribute("resetResultMsg", null);
	}
%>	
</head>
<body id="gradient">
	<!-- <div id="gradient" style="width:100%;height: 100%"> -->
		<div id="w" style="width:400px;height:200px;">
			<div style="margin-bottom:20px;margin-top: 20px;margin-left: 40px">
				<h1>修改密码</h1>
			</div>
			<form id="ff" method="post">
				<div style="margin-bottom:20px;margin-top: 20px;margin-left: 40px">
					<input id="oldpwd" class="easyui-passwordbox" prompt="请输入旧密码" name="oldpwd" iconWidth="28" style="width:80%;height:34px;padding:10px"  data-options="required:true,validType:'checkoldpwd'">
				</div>
				<div style="margin-bottom:20px;margin-top: 20px;margin-left: 40px">
					<input id="newpwd" class="easyui-passwordbox" prompt="请输入新密码" name="newpwd" iconWidth="28" style="width:80%;height:34px;padding:10px"  data-options="required:true,validType:'checknewpwd'">
				</div>
				<div style="margin-bottom:20px;margin-top: 20px;margin-left: 40px">
					<input id="confirmpwd" class="easyui-passwordbox" prompt="请确认新密码" name="confirmpwd" iconWidth="28" style="width:80%;height:34px;padding:10px"  data-options="required:true,validType:'checkconfirmpwd'">
				</div>
				<div style="padding:5px 0;margin-left: 40px" >
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px" >修改密码</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">清除</a>
				</div>  	
			</form>
			
		</div>
	<!-- </div> -->
	<script>
		function submitForm(){
			$('#ff').form('submit', {
		        onSubmit : function() {
		        	return $(this).form('enableValidation').form('validate');
		        },
		        success:function(data){      
		        if (data.success == 'ture') {   
		             $.messager.alert('提示',data.msg,'info');   
		        }else{   
		            //验证通过后跳转页面
		        	var form = document.forms[0];
		    		form.action="/weixin/AdminSet/resetPWD.do";
		    		form.method="post";
		    		form.submit();
		        }     
		      }  
		    });

		}
		function clearForm(){
			$('#ff').form('clear');
		}
	</script>

	
</body>
</html>