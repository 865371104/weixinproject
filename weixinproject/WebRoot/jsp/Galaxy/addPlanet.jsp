<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加行星</title>
</head>
	<link rel="stylesheet" type="text/css" href="../../css/registered.css">
	<link rel="stylesheet" type="text/css" href="../../jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../../jquery-easyi-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../../jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="../../jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="../../jquery-easyui-1.5/jquery.easyui.min.js"></script>
<body>
	<h>添加行星</h>
	<form name="PlanetForm" action="/weixin/galaxy/addPlanet.do" method="post">
		行星名称：<input type="text" name="planename"><br>
		行星半径：<input type="text" name="radius"><br>
		行星质量：<input type="text" name="quality"><br>
		轨道周期：<input type="text" name="cycle"><br>
		轨道半长轴：<input type="text" name="sma"><br>
		轨道离心率：<input type="text" name="cro"><br>
		轨道倾角:<input type="text" name="dipangle"><br>
		发现方法:<input type="text" name="dm"><br>
		发现年份:<input type="text" class="easyui-datebox" required="required">
				<script type="text/javascript">
			    	$.extend($.fn.datebox.defaults.formatter = function(date){
			    		var y = date.getFullYear();
			    		var m = date.getMonth()+1;
			    		var d = date.getDate();
			    		return y+'-'+m+'-'+d;
			  		});
			    	
			        $.fn.datebox.defaults.parser = function(s){
			        	var t = Date.parse(s);
			        	if (!isNaN(t)){
			        		return new Date(t);
			        	} else {
			        		return new Date();
			        	}
			        };
		    	</script><br>
		距离:<input type="text" name="distance"><br>
		官方编号:<input type="text" name="numvar"><br>
		行星图片：<input type="text" name="img"><br>
		行星描述：<input type="text" name="note"><br>
		所属星系：<select name="galaxys">
			<c:forEach items="${galaxys}" var="g"> 
				<option value="${g.galaxyid}">${g.galaxyName}</option> 
			</c:forEach> 
		</select> 
		<input type="submit" value="添加"><br>
	</form>
</body>
</html>