<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript">

</script>
</head>
<body>
	<h>编辑行星</h>
	<form name="planetForm" action="/weixin/galaxy/updatePlanet.do" method="post">
		<input type="hidden" name="planetid" value="${planet.planetid  }">
		行星名称：<input type="text" name="planename" value="${planet.planename }"><br>
		行星半径：<input type="text" name="radius" value="${planet.radius }"><br>
		行星质量：<input type="text" name="quality" value="${planet.quality }"><br>
		轨道周期：<input type="text" name="cycle" value="${planet.cycle }"><br>
		轨道半长轴：<input type="text" name="sma" value="${planet.sma }"><br>
		轨道离心率：<input type="text" name="cro" value="${planet.cro }"><br>
		轨道倾角:<input type="text" name="dipangle" value="${planet.dipangle }"><br>
		发现方法:<input type="text" name="dm" value="${planet.dm }"><br>
		发现年份:<input type="text" name="date" value="${planet.date }"><br>
		距离:<input type="text" name="distance" value="${planet.distance }"><br>
		官方编号:<input type="text" name="numvar" value="${planet.numvar }"><br>
		行星图片：<input type="text" name="img" value="${planet.img }"><br>
		行星描述：<input type="text" name="note" value="${planet.note }"><br>
		<input type="submit" value="编辑" >
		
		
	
	</form>
	
	
	
</body>
</html>