<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	function del(id){
		$.get("/weixin/galaxy/delGalaxy.do?id="+id,function(data){
			if("success" == data.result){
				alert("删除成功!");
				window.location.reload(); 
			}else{
				alert("删除失败!")
			}
		});
	}

</script>
</head>
<body>
	<a href="/weixin/jsp/Galaxy/addGalaxy.jsp">新建星系</a>
	<table border="1">
		<tbody>
			<tr>
				<th>数据库编号</th>
				<th>星系名称</th>
				<th>官方星系编号</th>
				<th>星系缩略图</th>
				<th>发现日期</th>
				<th>备注</th>
				<th>内部行星</th>
				<th>操作</th>
			</tr>
			<c:if test="${!empty galaxy }">
			<c:forEach items="${galaxy }" var="g">
			<tr>
				<td>${g.galaxyid }</td>
				<td>${g.galaxyName }</td>
				<td>${g.galaxyIN }</td>
				<td>${g.img }</td>
				<td>${g.fyear }</td>
				<td>${g.note }</td>
				<td>
					<a href="/weixin/galaxy/getPlanetbyGa.do?galaxy=${g.galaxyid }">查看内部星系</a>
				</td>
				<td>
					<a href="/weixin/galaxy/getGalaxytoedit.do?id=${g.galaxyid}">编辑</a>
					<a href="javascript:del('${g.galaxyid}')">删除</a>
				</td>
			</tr>
			</c:forEach>
			</c:if>
			<c:if test="${empty galaxy }">
			<tr>
				<td colspan="8" align="center">没有记录</td>
			</tr>
			</c:if>
		</tbody>
	</table>
</body>
</html>