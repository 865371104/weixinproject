<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>行星列表</title>
<script type="text/javascript">
	function del(id){
		$.get("/weixin/galaxy/delPlanet.do?id=" + id,function(data){
			if("success" == data.result){
				alert("删除成功!");
				window.location.reload(); 
			}else{
				alert("删除失败!")
			}
		});
	}
	function gradeChange(id){
		if("" == id){
			var Url = "/weixin/galaxy/getAllPlanet.do?";
		}else{
			var Url = "/weixin/galaxy/getPlanetbyGa.do?galaxy="+id;
		}
		window.navigate(Url); 
    }
</script>
</head>
<body>
	
	<table border="1">
		<tbody>
			<tr>
				<td colspan="5">
					<a href="/weixin/galaxy/toAddPlanet.do">新建行星</a>
				</td>
				<td colspan="5">
				</td>
				<td colspan="5">
					按星系显示：
					<select name="galaxys" onchange="gradeChange(this.options[this.options.selectedIndex].value);">
						<option value=''>---全部星系---</option> 
						<c:forEach items="${galaxys}" var="g"> 
							<option value="${g.galaxyid}" ${g.galaxyid==galaxy?'selected':''}>${g.galaxyName}</option> 
						</c:forEach> 
					</select> 
					<a href="/weixin/galaxy/getAllGalaxy.do">返回星系列表</a>
				</td>
			</tr>
			<tr>
				<td>数据库编号</td>
				<td>行星名称</td>
				<td>行星半径</td>
				<td>行星质量</td>
				<td>轨道周期</td>
				<td>轨道半长轴</td>
				<td>轨道离心率</td>
				<td>轨道倾角</td>
				<td>距离</td>
				<td>发现方法</td>
				<td>发现年份</td>
				<td>官方编号</td>
				<td>行星图片</td>
				<td>行星描述</td>
				<td>操作</td>
			</tr>
			<c:if test="${!empty planet }">
			<c:forEach items="${planet }" var="p">
			<tr>
				<td>${p.planetid }</td>
				<td>${p.planename }</td>
				<td>${p.radius }</td>
				<td>${p.quality }</td>
				<td>${p.cycle }</td>
				<td>${p.sma }</td>
				<td>${p.cro }</td>
				<td>${p.dipangle }</td>
				<td>${p.distance }</td>
				<td>${p.dm }</td>
				<td>${p.date }</td>
				<td>${p.numvar }</td>
				<td>${p.img }</td>
				<td>${p.note }</td>
				<td>
					<a href="/weixin/galaxy/getPlanettoedit.do?id=${p.planetid}">编辑</a>
					<a href="javascript:del('${p.planetid}')">删除</a>
				</td>
			</tr>
			</c:forEach>
			</c:if>
			<c:if test="${empty planet }">
			<tr>
				<td colspan="15" align="center">没有记录</td>
			</tr>
			</c:if>
		</tbody>
	</table>
</body>
</html>