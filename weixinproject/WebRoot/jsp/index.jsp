<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin Window</title>
	<link rel="stylesheet" type="text/css" href="../css/index.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyi-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.easyui.min.js"></script>
</head>

<body class="easyui-layout">  

	<!-- 上面部分 -->
	<div data-options="region:'north',border:false" style="height:60px;background:#B3DFDA;padding:10px">
		<div class="welcome"><h2>仰望星空系统后台管理</h2></div>
		<div class="logout">欢迎你,${administrators.adname } 注销</div>
		<script type="text/javascript">
		</script>
	</div>
	
	<!-- 左面部分 -->
	<div data-options="region:'west',split:true,title:'West'" style="width:200px;">
		<iframe scrolling="no" frameborder="0" src="/weixin/List/getList.do" name="Westconter" width="100%" height="99%" seamless></iframe>
	</div>
	<script>
		
	</script>
	
	<!-- 右面部分 -->
	
	<div data-options="region:'east',split:true,collapsed:true,title:'East'" style="width:100px;padding:10px;">
		动作消息日志
	</div>
	
	<!-- 下面部分 -->
	
	<div data-options="region:'south',border:false" style="height:50px;background:#A9FACD;padding:10px;text-align: center;">
		制作团队：TOE工作室
	</div>
	
	<!-- 中间部分 -->
	
	<div data-options="region:'center',title:'Center'" style="overflow:hidden;">
		<iframe scrolling="auto" noresize="noresize" frameborder="0" src="info.html" name="conter" width="100%" height="100%" seamless></iframe>
	</div>
	
</body>
</html>