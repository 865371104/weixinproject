<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/my" prefix="my"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>权限列表</title>
	<link rel="stylesheet" type="text/css" href="../css/login.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyi-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/customVerification.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
	function Rerole(){
		$('#Rerole-win').window('open');
	}
	
	function outcheck(){
		$.ajax({
        	type : 'post',
        	async : false,
        	url : 'http://localhost:8080/weixin/role/delAdminRole.do',
        	data : {
        	},
        	success : function(data) {
        	}
    	});
	}
	
	function delAdminRole(id){
		$.ajax({
        	type : 'post',
        	async : false,
        	url : 'http://localhost:8080/weixin/role/delAdminRole.do',
        	data : {
            	"id" : id
        	},
        	success : function(data) {
        		if("success" == data.result){
    				alert("撤除成功!");
    				window.location.reload()
    			}else{
    				alert("撤除失败!")
    			}
        	}
    	});
	}
	</script>
<%	
	if(session.getAttribute("updateadminrole")!=null){
%>
	<script type="text/javascript">
		alert("${updateadminrole }");
	</script>
<%			
		session.setAttribute("updateadminrole", null);
	}
%>

<style type="text/css">
	.td{border:1px #000000 solid;}
	.table{border-collapse:collapse;background:#ffffff;}
</style>	
</head>
<body id="gradient">
	<div id="gradient" style="width:100%;height: 100%;">
		<table class="table">
			<tbody>
				<tr>
					<th width="100px" class="td">数据库编号</th>
					<th width="100px" class="td">管理员</th>
					<th width="200px" class="td">操作</th>
				</tr>
				<c:if test="${!empty administratorss }">
				<c:forEach items="${administratorss }" var="a">
				<tr>
					<td align="center" class="td">${a.adminid }</td>
					<td align="center" class="td">${a.adname }</td>
					<td align="center" class="td">
						<a href="/weixin/role/toReAdminRole.do?id=${a.adminid}" onclick="Rerole()" target="rerole-conter">改变权限</a>
						<c:if test="${a.role.role!='2' }">
							<a href="javascript:delAdminRole('${a.adminid}')">撤除</a>
						</c:if>
					</td>
				</tr>
				</c:forEach>
				</c:if>
				<c:if test="${empty administratorss }">
				<tr>
					<td colspan="8" align="center" class="td">没有记录</td>
				</tr>
				</c:if>
			</tbody>
		</table>
	</div>
	
	<div id="Rerole-win" class="easyui-window" resizable="false" maximizable="false" minimizable="false" collapsible="false" title="改变权限" closed="true" style="width:400px;height:150px;overflow:hidden;">
		<iframe scrolling="no" noresize="noresize" frameborder="0" src="" name="rerole-conter" width="100%" height="100%" seamless></iframe>
	</div>
	
</body>
</html>