<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>权限列表</title>
	<link rel="stylesheet" type="text/css" href="../css/login.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/customVerification.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
	function SeeAuthority(){
		$('#Authority-win').window('open');
	}
	function Rename(){
		$('#Rename-win').window('open');
	}
	
	function delRole(id){
		$.ajax({
        	type : 'post',
        	async : false,
        	url : 'http://localhost:8080/weixin/role/delRole.do',
        	data : {
            	"id" : id
        	},
        	success : function(data) {
        		if("success" == data.result){
    				alert("删除成功!");
    				$('#tt').datagrid('reload');
    			}else{
    				alert("删除失败!")
    			}
        	}
    	});
	}
	
	
	</script>
		
</head>
<body id="gradient" style="overflow:hidden;">
	<div id="gradient" style="width:100%;height: 100%;">
		<table id="tt" class="easyui-datagrid" style="width:800px;height:500px"
				singleSelect = true
				url="toAllRoleList.do"
				title="管理权限"
				toolbar="#tb">
			<thead>
				<tr>
					<th field="role" width="150px">角色编码</th>
					<th field="content" width="250px">角色名</th>
					<th data-options="field:'_operate1',width:120,align:'center',formatter:formatOper1">角色权限</th> 
					<th data-options="field:'_operate2',width:120,align:'center',formatter:formatOper2">拥有此角色的管理员</th>
					<th data-options="field:'_operate3',width:120,align:'center',formatter:formatOper3">操作</th>
				</tr>
			</thead>
		</table>
				<div id="tb">
				<a href="/weixin/role/toaddRole.do" target="addrole-conter" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:addRole()">添加角色</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="javascript:$('#tt').datagrid('reload')">刷新表格</a>
		</div>

<script type="text/javascript">	
		function addRole(){
			$('#addrole-win').window('open');
		}
		function formatOper1(val,row,index){
			if(row.role==1||row.role==2){
				return '操作角色权限';   	
			}else{
				return '<a href="/weixin/role/toAuthority.do?id='+row.role+'" onclick="SeeAuthority()" target="Authority-conter">操作角色权限</a>'; 
			}
		}  
		function formatOper2(val,row,index){ 			
			return '<a href="/weixin/role/toAdministrators.do?id='+row.role+'" onclick="SeeAuthority()" target="Authority-conter">操作管理员组</a>';   			
		} 
		function formatOper3(val,row,index){
			if(row.role==1||row.role==2){
				return '重命名 删除';   	
			}else{
				return '<a href="/weixin/role/toRenameRole.do?id='+row.role+'" onclick="Rename()" target="rename-conter">重命名</a>&nbsp'+
				'<a href="#" onclick="delRole('+row.role+')">删除</a>';    
			}
    		return '<a href="/weixin/role/toRenameRole.do?id='+row.role+'" onclick="Rename()" target="rename-conter">重命名</a>&nbsp'+
    				'<a href="#" onclick="delRole('+row.role+')">删除</a>';  
		} 
</script>			
		
	</div>
	
	<div id="Authority-win" class="easyui-window" resizable="false" title=" " closed="true" style="width:500px;height:500px;overflow:hidden;">	
		<iframe scrolling="no" noresize="noresize" frameborder="0" src="" name="Authority-conter" width="100%" height="100%" seamless></iframe>
	</div>
	
	<div id="addrole-win" class="easyui-window" resizable="false" title="新建角色" closed="true" style="width:400px;height:300px;overflow:hidden;">	
		<iframe scrolling="no" noresize="noresize" frameborder="0" src="" name="addrole-conter" width="100%" height="100%" seamless></iframe>
	</div>
	
	<div id="Rename-win" class="easyui-window" resizable="false" maximizable="false" minimizable="false" collapsible="false" title="重命名" closed="true" style="width:400px;height:400px;overflow:hidden;">
		<iframe scrolling="no" noresize="noresize" frameborder="0" src="" name="rename-conter" width="100%" height="100%" seamless></iframe>
	</div>
	
	

	
</body>
</html>