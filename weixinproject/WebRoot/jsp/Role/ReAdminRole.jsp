<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>更改权限</title>
	<link rel="stylesheet" type="text/css" href="../css/login.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyi-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/customVerification.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/locale/easyui-lang-zh_CN.js"></script>
		
</head>
<body id="gradient">
	<form id="ff" method="post">
		<input type="hidden" name="admin" value="${admin.adminid }">
		<select id="role" class="easyui-combobox" name="role" name="权限" style="width:200px;height:34px;padding:10px;" panelHeight="50">
			<c:forEach items="${roles}" var="r"> 
				<c:if test="${r.role!=2}">
				<option value="${r.role}" ${r==admin.role?'selected':''}>${r.content}</option> 
				</c:if>
			</c:forEach>
		</select>
		<br><br>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:70px">更改权限</a>
	</form>	
	
	<script>
		function submitForm(){
			var form = document.forms[0];
			form.action="/weixin/role/ReAdminRole.do";
    		form.method="post";
    		form.submit();
    		parent.$('#Rerole-win').window('close');
    		parent.window.location.reload(); 
		}

	</script>
	
</body>
</html>