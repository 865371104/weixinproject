<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/my" prefix="my"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>权限列表</title>
	<link rel="stylesheet" type="text/css" href="../css/login.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyi-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/customVerification.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/locale/easyui-lang-zh_CN.js"></script>
	
		
</head>
<body id="gradient">
	<form>
			<table>
			<c:if test="${!empty AllParentAuthority }">
			<c:forEach items="${AllParentAuthority }" var="a">
			<tr>
				<td>${a.authorityName }<input type="checkbox" name="${a.authorityId}" <c:if test="${my:IsContain(Authority,a)}">checked="checked"</c:if>/>${a.authorityId}</td>
				<td></td>
			</tr>
			<tr height="40px">
				<td></td>
				<td>
					<table>
						<tr>
							<c:if test="${!empty AllAuthority }">
							<c:forEach items="${AllAuthority }" var="b">
								<c:if test="${b.authorityparent.authorityId == a.authorityId }">
								<td>${b.authorityName }<input type="checkbox" name="${a.authorityId}" <c:if test="${my:IsContain(Authority,a)}">checked="checked"</c:if>/>${a.authorityId}</td>
								</c:if>
							</c:forEach>
							</c:if>
						</tr>
					</table>
				</td>
			</tr>
			</c:forEach>
			</c:if>
			</table>
		</form>
	
	<script>
		function submitForm(){
			window.location.href="/weixin/jsp/Adminmsg/editAdminMsg.jsp"; 
		}

	</script>

	
</body>
</html>