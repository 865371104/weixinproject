<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%	
	if(session.getAttribute("updateroleContent")!=null){
%>
	<script type="text/javascript">
		alert("${updateroleContent }");
	</script>
<%			
		session.setAttribute("updateroleContent", null);
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>权限列表</title>
	<link rel="stylesheet" type="text/css" href="../css/login.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyi-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/customVerification.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/locale/easyui-lang-zh_CN.js"></script>
		
	<script type="text/javascript">	
	$.extend($.fn.validatebox.defaults.rules, {
	    checkContent : {
	        validator : function(value, param) {
	            var haha = " ";
	            $.ajax({
	                type : 'post',
	                async : false,
	                url : 'http://localhost:8080/weixin/role/checkContent.do',
	                data : {
	                    "name" : value
	                },
	                success : function(data) {
	                    haha = data;
	                }
	            });
	            return haha.indexOf("true");
	        },
	        message : '已经存在同名的角色名'
	    }
	});

	function check(){
		var name = $("#content").val().trim();
		$.ajax({
        	type : 'post',
        	async : false,
        	url : 'http://localhost:8080/weixin/role/check.do',
        	data : {
            	"name" : name
        	},
        	success : function(data) {
        	}
    	});
		submitForm();
    	outcheck();
	}
	function outcheck(){
		$.ajax({
        	type : 'post',
        	async : false,
        	url : 'http://localhost:8080/weixin/role/outcheck.do',
        	data : {
        	},
        	success : function(data) {
        	}
    	});
	}
	</script>	
</head>
<body id="gradient">
	<form id="ff" method="post">
		<input type="hidden" name="role" value="${role.role }">
		<input  id="content" class="easyui-textbox" name="content" prompt="角色名" iconWidth="28" style="width:200px;height:34px;padding:10px;"  data-options="required:true,validType:'checkContent'">
		<br><br>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="check()" style="width:70px">重命名</a>
	</form>	
	
	<script>
		function submitForm(){
			$('#ff').form('submit', {
		        onSubmit : function() {
		        	return $(this).form('enableValidation').form('validate');
		        },
		        success:function(data){      
		        if (data.success == 'ture') {   
		             $.messager.alert('提示',data.msg,'info');   
		        }else{   
		            //验证通过后跳转页面
		        	var form = document.forms[0];
		    		form.action="/weixin/role/RenameRole.do";
		    		form.method="post";
		    		form.submit();
		    		parent.$('#tt').datagrid('reload'); 
		    		parent.$('#Rename-win').window('close');
		        }     
		      }  
		    });
		}

	</script>
	
</body>
</html>