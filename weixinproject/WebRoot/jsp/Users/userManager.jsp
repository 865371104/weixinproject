<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>用户信息</title>
		<link rel="stylesheet" type="text/css" href="../../jquery-easyui-1.5/themes/default/easyui.css">
		<link rel="stylesheet" type="text/css" href="../../jquery-easyui-1.5/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="../../jquery-easyui-1.5/demo.css">
		<script type="text/javascript" src="../../jquery-easyui-1.5/jquery.min.js"></script>
		<script type="text/javascript" src="../../jquery-easyui-1.5/jquery.easyui.min.js"></script>
	</head>
	<body>
		<h2>Basic DataGrid</h2>
		<p>The DataGrid is created from markup, no JavaScript code needed.</p>
		<div style="margin:20px 0;"></div>
	
		<table class="easyui-datagrid" title="用户信息管理" style="width:800px;height:500px"
				data-options="singleSelect:true,collapsible:true,url:'datagrid_data1.json',method:'get'">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th data-options="field:'userId',width:70">数据库编号</th>
					<th data-options="field:'openId',width:60">OpenId</th>
					<th data-options="field:'userName',width:80">用户名</th>
					<th data-options="field:'passWard',width:80">密码</th>
					<th data-options="field:'role.content',width:80">权限</th>
					<th data-options="field:'birthday',width:80,align:'center'">生日</th>
					<th data-options="field:'email',width:80,align:'center'">邮箱</th>
					<th data-options="field:'sex',width:40,align:'center'">性别</th>
					<th data-options="field:'nickName',width:80,align:'center'">昵称</th>
					<th data-options="field:'astate',width:100,align:'center'">是否关注公众号</th>
				</tr>
			</thead>
		</table>
	 
	</body>

</html>