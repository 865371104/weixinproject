<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>文章类型列表</title>
	<link rel="stylesheet" type="text/css" href="../css/login.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/customVerification.js"></script>
	<script type="text/javascript" src="../jquery-easyui-1.5/locale/easyui-lang-zh_CN.js"></script>
	
	<script type="text/javascript">
	
	function addAstyle(){
		$('#addAstyle-win').window('open');
	}
	function editAstyle(){
		var row = $('#tt').datagrid('getSelected');
		if(row){
			var id = row.astyleId
			$('#editAstyle-win').window('open');
			return "/weixin/astyle/editAstyle.do?id="+id;
		}else{
			return "#";
		}
	}
	
	function delastyle(){
		var row = $('#tt').datagrid('getSelected');
		if(row){
			var id = row.astyleId
			$.ajax({
	        	type : 'post',
	        	async : false,
	        	url : 'http://localhost:8080/weixin/astyle/delAstyle.do',
	        	data : {
	            	"id" : id
	        	},
	        	success : function(data) {
	        		if("success" == data.result){
	    				alert("删除成功!");
	    				$('#tt').datagrid('reload');
	    			}else{
	    				alert("删除失败!")
	    			}
	        	}
	    	});
		}
	}
	
	</script>
	
</head>
<body id="gradient">
	<table id="tt" class="easyui-datagrid" style="width:800px;height:500px"
				rownumbers = true,
				singleSelect = true
				url="toAllAstyleList.do"
				title="文章类别"
				toolbar="#tb">
		<thead>
			<tr>
				<th data-options="field:'sname',width:150">文章类型</th>
				<th data-options="field:'articlesSize',width:150">文章书目</th>
				<th data-options="field:'weixinUserInfosSize',width:150">关注人数</th>
			</tr>
		</thead>
	</table>
	  
	<div id="tb">
		<a href="/weixin/astyle/toAddAstyle.do" target="addAstyle-conter" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:addAstyle()">添加</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:delastyle()">删除</a>
		<a href="javascript:editAstyle()" target="editAstyle-conter" class="easyui-linkbutton" iconCls="icon-edit" plain="true">编辑</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="javascript:$('#tt').datagrid('reload')">刷新</a>
	</div> 
	
	<div id="addAstyle-win" class="easyui-window" resizable="false" title="新建类别" closed="true" style="width:500px;height:300px;overflow:hidden;">	
		<iframe scrolling="no" noresize="noresize" frameborder="0" src="" name="addAstyle-conter" width="100%" height="100%" seamless></iframe>
	</div>
	
	<div id="editAstyle-win" class="easyui-window" resizable="false" title="修改类别" closed="true" style="width:500px;height:300px;overflow:hidden;">	
		<iframe id="editAstyle-conter" scrolling="no" noresize="noresize" frameborder="0" src="" name="editAstyle-conter" width="100%" height="100%" seamless></iframe>
	</div>
	
</body>
</html>