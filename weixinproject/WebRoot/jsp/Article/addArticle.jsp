<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="../../js/jquery-1.7.1.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加文章</title>

</head>

<body>
	<h1>添加文章</h1>
	<form name="ArticleForm" action="/weixin/article/addArticle.do" method="post">
		文章标题：<input type="text" name="title" size="40"><br>
		文章正文：<br><textarea name="content" cols="50" rows="25"></textarea><br>
		编辑作者：<input type="text" name="editor"><br>
		发布组织：<input type="text" name="institutions"><br>
		备注:<input type="text" name="note"><br>
		文章类别：<select name="astyle">
			<c:forEach items="${astyles}" var="a"> 
				<option value="${a.astyleId}">${a.sname}</option> 
			</c:forEach> 
		</select> 
		<input type="submit" value="发表文章"><br>
	</form>
</body>
</html>