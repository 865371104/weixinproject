<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>文章：${article.title}</title>

<script type="text/javascript"> 
    var xmlHttp; 
    //创建xmlHttp对象 
	function createXMLHttpRequest(){ 
        if(window.ActiveXObject){ 
           xmlHttp = new ActiveXObject("Microsoft.XMLHTTP"); 
        } 
    	else if(window.XMLHttpRequest){ 
       		xmlHttp = new XMLHttpRequest(); 
    	} 
	} 
	function startRequest(){ 
    	createXMLHttpRequest(); 
   		xmlHttp.onreadystatechange = handleStateChange;   //回调函数 
    	xmlHttp.open("GET","${article.articleContent}",true);     
    	xmlHttp.send(null); 
	} 

	function handleStateChange(){ 
    	if(xmlHttp.readyState == 1){ 
        	document.getElementById("s").innerHTML = "正在加载……"; 
    	} 
		if(xmlHttp.readyState == 2){ 
     	   document.getElementById("s").innerHTML = "已加载!"; 
    	} 
    	if(xmlHttp.readyState == 4){ 
        	if(xmlHttp.status == 200){//成功加载 
            	document.getElementById("s").innerHTML = xmlHttp.responseText;//返回aaa.txt文件的内容，实际应用中可以在servlet中把要输出的内容写入输出流. 
        	} else{
        		document.getElementById("s").innerHTML = xmlHttp.status;
        	}
    	} 
	} 
</script> 
</head>
<body onload="startRequest();">
	<h1 align="center">${article.title}</h1>
	<h1 align="right">${article.editor}</h1>
	<div id="s"></div>
</body>
</html>