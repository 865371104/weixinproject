<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Add user</title>
	<link rel="stylesheet" type="text/css" href="../../jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../../jquery-easyi-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../../jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="../../jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="../../jquery-easyui-1.5/jquery.easyui.min.js"></script>
</head>
<body>
	<form id="" action="" method="post">
		<div style="margin-bottom:20px;margin-top: 20px;margin-left: 70px">
				<input class="easyui-textbox" prompt="请输入新用户名" iconWidth="28" style="width:80%;height:34px;padding:10px;">
			</div>
			<div style="margin-bottom:20px;margin-left: 70px">
 				<input id="pwd" name="pwd" class="easyui-passwordbox"  prompt="请输入密码" iconWidth="28" style="width:80%;height:34px;padding:10px" data-options="validType:'minLength[8]'">
				<script type="text/javascript">
					$.extend($.fn.validatebox.defaults.rules, {
						minLength: {
							validator: function(value, param){
								return value.length >= param[0];
							},
							message: '请至少输入8位字符密码'
						}
					});
				</script>
			</div>
			<div style="margin-bottom:20px;margin-left: 70px">
				<input id="rpwd" name="rpwd" class="easyui-passwordbox" prompt="请再次输入密码" iconWidth="28" style="width:80%;height:34px;padding:10px" validType="equals['#pwd']">										
				<script type="text/javascript">
		  	  		// extend the 'equals' rule
		   	 		$.extend($.fn.validatebox.defaults.rules, {
		     			equals: {
		    				validator: function(value,param){
		    					return value == $(param[0]).val();
		    				},
		    				message: '密码不一致'
		        		}
		    		});
				</script>
			</div>
			
			
			<div style="text-align:center;padding:5px 0">
				<a href="files/registereduser.jsp" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">下一步</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">清除</a>
			</div>  
	</form>
	<script>
		function submitForm(){
			$('#ff').form('submit');
		}
		function clearForm(){
			$('#ff').form('clear');
		}
		
	</script>
</body>
</html>