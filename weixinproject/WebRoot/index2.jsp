<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin Window</title>
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="jquery-easyui-1.5/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="jquery-easyi-1.5/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="jquery-easyui-1.5/demo/demo.css">
	<script type="text/javascript" src="jquery-easyui-1.5/jquery.min.js"></script>
	<script type="text/javascript" src="jquery-easyui-1.5/jquery.easyui.min.js"></script>
</head>

<body class="easyui-layout">  

	<!-- 上面部分 -->
	<div data-options="region:'north',border:false" style="height:60px;background:#B3DFDA;padding:10px">
		<div class="welcome">欢迎${user.nickname}<!-- 显示用户的昵称 -->来到后台管理</div>	
		<div class="logout">注销</div>
		<script type="text/javascript">
		
		</script>
	</div>
	
	<!-- 左面部分 -->
	
	<div data-options="region:'west',split:true,title:'West'" style="width:150px;">
		<div id="aa" class="easyui-accordion menubutton" style="height:100%;">
       		<div title="用户" data-options="iconCls:'icon-save'" style="padding:10px;">
    			<ul id="tt" class="easyui-tree">
    				<li>
    					添加用户
    				</li>
    				<li>
    					删除用户
    				</li>
    				<li>
    					修改用户
    				</li>
    				<li>
    					查询用户
    				</li>
    			</ul>
       		</div>
        	<div title="星星" data-options="iconCls:'icon-reload',selected:true" style="padding:10px;">
    			<ul id="tt" class="easyui-tree">
    				<li>
    					增	
    				</li>
    				<li>
    					删
    				</li>
    				<li>
    					改
    				</li>
    				<li>
    					查
    				</li>
    			</ul>
        	</div>
        	<div title="信息" data-options="selected:true" style="padding:10px;">
    			发布
        	</div>
    	</div>
	</div>
	
	<script>
   		$('#aa').accordion({
   	    	animate:false
    	});
    	var pp = $('#aa').accordion('getSelected'); // 获取选中的面板（panel）
    	if (pp){
        	pp.panel('refresh','new_content.php'); // 调用 'refresh' 方法加载新内容
    	}	
	</script>
	
	<!-- 右面部分 -->
	
	<div data-options="region:'east',split:true,collapsed:true,title:'East'" style="width:100px;padding:10px;">
		动作消息日志
	</div>
	
	<!-- 下面部分 -->
	
	<div data-options="region:'south',border:false" style="height:50px;background:#A9FACD;padding:10px;text-align: center;">
		制作团队：TOE工作室
	</div>
	
	<!-- 中间部分 -->
	
	<div data-options="region:'center',title:'Center'">
		<div data-options="iconCls:'icon-reload',selected:true" style="padding: 10px;">
		</div>
	</div>
	
</body>
</html>